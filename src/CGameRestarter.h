#pragma once
#include "CState.h"

class CGame;
class CGameRestarter : public CState
{
public:
    explicit CGameRestarter(CGame * game);
    void RunState() override;
    bool ChangeState(ESTATE &state) override;
private:
    CGame * m_Game;
};
