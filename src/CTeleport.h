#pragma once
#include "CGameObject.h"
#include "EDir.h"

class CTeleport : public CGameObject
{
public:
    CTeleport(int x, int y, int destX, int destY);
    void SetType(DIR d);

    /*! Sends moving object to destination */
    void Collides(CMovingObject *p) override;
private:
    /*! Destination to teleport moving object */
    int m_DestX, m_DestY;
};