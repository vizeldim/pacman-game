#pragma once
#include "CObject.h"

class CScore : public CObject
{
public:
    CScore();
    void Draw() override;
    void operator +=(int i);
    /*! Sets score to zero */
    void ResetScore();

    int GetScore() const;
private:
    int m_Score;
};
