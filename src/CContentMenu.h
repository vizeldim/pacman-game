#pragma once
#include "CMenu.h"

class CContentMenu : public CMenu
{
public:
    CContentMenu(std::string  path, const SDL_Color &bgColor, int opacity);
    void RunState() override;
    /*! Read numbers from file until eof */
    void PrepareState() override;
    /*! Shows number values from file */
    void ShowContent();
private:
    std::string m_FilePath;
    /*! Holds values readen from file */
    std::vector<int> m_Content;
};
