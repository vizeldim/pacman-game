#include <sstream>
#include <iostream>
#include "CGame.h"
#include "CInputManager.h"
#include "CGraphicsManager.h"
#include "COverlayTitle.h"
#include "CPlayer.h"

using namespace std;

CGame::CGame() : m_CurrentLevel(0)
{
    SDL_Color black = {0,0,0,0};
    AddGameTitle(GAMEOVER,  new COverlayTitle(black, 177) );
    AddGameTitle(NEXTLEVEL, new COverlayTitle(black, 177) );
    AddGameTitle(WIN,       new COverlayTitle(black, 177) );
}

CGame::~CGame()
{
    for(auto t : m_Titles)
    { delete t.second; }
}

void CGame::RunState()
{
    if(m_LevelMaps.size()>m_CurrentLevel)
    {
        m_ObjectManager.UpdateObjects();
        m_ObjectManager.DrawObjects();
    }
}

void CGame::ConfigureState()
{
    if(m_LevelMaps.size()-1 < m_CurrentLevel)
    { cout << "NO MAP DISPOSABLE" << endl; }
    else
    {
        if(!( m_ObjectManager.ReadMap(m_LevelMaps[m_CurrentLevel]) ))
        {
            if(m_LevelMaps.size()>m_CurrentLevel+1)
            { NextLevel(); }
        }
        else if(!m_ObjectManager.ReadConfig(m_LevelConfigs[m_CurrentLevel]))
        { m_ObjectManager.SetDefaultConfig(); }
    }
}

bool CGame::ChangeState(ESTATE &state)
{
    if(m_ObjectManager.Win())
    {
        if(m_LevelMaps.size()<=m_CurrentLevel+1)
        {
            m_Titles[WIN] -> Show();
            SaveScore(m_ObjectManager.GetScore());
            RestartGame();
            m_Titles[WIN] -> Wait();
            state = MENU;
        }
        else
        {
            m_Titles[NEXTLEVEL] -> Show();
            NextLevel();
            m_Titles[NEXTLEVEL] -> Wait();
            state = GAME;
        }
    }
    else if(m_ObjectManager.Loose())
    {
        m_Titles[GAMEOVER] -> Show();
        RestartGame();
        m_Titles[GAMEOVER] -> Wait();
        state = MENU;
    }
    else if(CInputManager::GetInstance().m_Pause)
    {
        m_ObjectManager.Pause();
        CInputManager::GetInstance().m_Pause=false;
        state = PAUSE;
    }
    else{return (CInputManager::GetInstance().m_Quit);}
    return true;
}

void CGame::RestartGame()
{
    m_CurrentLevel = 0;
    m_ObjectManager.Clear();
    m_ObjectManager.Restart();
    ConfigureState();
}

void CGame::NextLevel()
{
    ++m_CurrentLevel;
    m_ObjectManager.Clear();
    ConfigureState();
}

void CGame::AddGameTitle(EGAMETITLE state, COverlayTitle *title)
{ m_Titles.insert(make_pair(state,title)); }

void CGame::PrepareState()
{
    cout << "RENDER MAP TEXTURE" << endl;
    CGraphicsManager::GetInstance().ClearRenderer();
    CGraphicsManager::GetInstance().Render(m_ObjectManager.m_Background);

    m_CountDown.Run();
    CState::PrepareState();
    CGraphicsManager::GetInstance().ClearRenderer();
    CGraphicsManager::GetInstance().DrawOnWindow();
    CGraphicsManager::GetInstance().Render(m_ObjectManager.m_Background);
    m_ObjectManager.m_Player->DrawSH();

    m_ObjectManager.UnPause();
}

void CGame::LoadLevelInfo(const string& levelsPath)
{
    std::ifstream infile(levelsPath);
    if(!infile.is_open())
    { throw runtime_error("LEVELS FILE COULD NOT BE OPENED."); }

    std::string line;
    bool fail = false;
    while (std::getline(infile, line))
    {
        std::istringstream iss(line);
        std::string mapPath, configPath;
        if (!(iss >> mapPath >> configPath)) // error
        {
            fail = true;
            break;
        }
        m_LevelMaps.push_back(mapPath);
        m_LevelConfigs.push_back(configPath);
    }

    if(fail || infile.bad())
    { throw runtime_error("LEVELS FILE READING FAILED."); }
}

SDL_Texture * CGame::GetBackground()
{ return m_ObjectManager.GetBackground(); }

void CGame::StartStateFrame() {}

void CGame::SaveScore(int scoreValue)
{
    cout << "-------------SAVING SCORE-------------" << scoreValue << endl;
    fstream scoreFile(m_ScoreFilePath, fstream::in | fstream::out | fstream::app);
    if(!scoreFile.is_open())
    {
        cout << "Cant Open scores file. :(" << endl;
        scoreFile.close();
    }
    else
    {
        int scores[3] = {0,0,0};
        bool readalbe = true;
        for (int & score : scores)
        {
            scoreFile >> score;
            if(scoreFile.eof())
            { break; }
            else if(scoreFile.fail())
            {
                readalbe = false;
                cout << "Cant read scores. :(" << endl;
            }
        }
        scoreFile.close();

        if(readalbe && scores[2] < scoreValue)
        {
            if( remove( m_ScoreFilePath.c_str() ) != 0 )
            { cout << "Could not delete file." << endl; }
            else
            {
                scoreFile.open(m_ScoreFilePath, fstream::in | fstream::app);
                if(!scoreFile.is_open())
                {
                    cout << "Cant rewrite scores file. :(" << endl;
                    scoreFile.close();
                }
                else
                {
                    int val = 0;
                    for (int score : scores)
                    {
                        if(score < scoreValue)
                        {
                            val = scoreValue;
                            scoreValue = 0;
                        }
                        else
                        { val = score; }
                        if(!(scoreFile << val << endl))
                        { cout << "Cant write your score to scores file. :( "; }
                    }
                }
                scoreFile.close();
            }
        }
    }
}

void CGame::SetScoreFile(const string& path)
{ m_ScoreFilePath = path; }

void CGame::SetOverlayTitle(EGAMETITLE title, const string & text)
{ m_Titles[title]->SetTitle(text); }
