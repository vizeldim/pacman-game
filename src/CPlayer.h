#pragma once
#include <string>
#include <vector>
#include "EDir.h"
#include "CMovingObject.h"
#include "CScore.h"
#include "CHealth.h"

class CGhost;
class CPlayer : public CMovingObject
{
public:
    CPlayer(SDL_Texture *tex, int x, int y, int  health);
    void Update() override;
    void Draw() override;
    void HandleCollision(CGameObject * object) override;

    /*! Set Players start position */
    void SetPlayer(SDL_Texture * tex, int x, int y);

    /*! Sets player position to default map position (start position) */
    void DefaultPos() override;

    /*! Draw Score && Health and manipulate with them */
    void DrawSH();
    void IncScore(int i);
    void DecHealth();
    int GetHealth();

    /*! Returns amount of eaten food */
    int EatenAmount() const;

    /*! Increases amount of eaten food */
    void Eat();

    /*! Adds texture into m_Animations container */
    void AddAnimation(SDL_Texture * tex);

    /*! Resets player for the new level */
    void ResetInfo();

    /*! Changes enemies(ghosts) to frightened mode */
    void Undefeatable();

    /*! Calls ResetScore on score => sets to zero */
    void ResetScore();

    int GetScore();

    void SetSHposX(int scoreX, int healthX);

    std::vector<CGhost *> m_Enemies;

    void HideOnBg(CGameObject *obj);

private:
    void CheckCollisions() override;

    /*! Players current Score && Health */
    CScore m_Score;
    CHealth m_Health;

    /*! Amount of eaten food */
    int m_EatenAmount;

    /*! Animation components -> changes current texture depended on animation speed */
    void Animate();
    std::vector<SDL_Texture *> m_Animations;
    int m_AnimationSpeed;
    int m_Frame;
    int m_CurrentAnimation;

    /*! Rotation of a player based on a current direction
     *  -> Rotate() sets angle
     */
    void Rotate();
    int m_Angle{};
};
