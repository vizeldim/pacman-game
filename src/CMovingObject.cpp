#include <iostream>
#include "CMovingObject.h"
#include "CGraphicsManager.h"
#include "CObjectManager.h"

using namespace std;

CMovingObject::CMovingObject(SDL_Texture *tex, int x, int y) : CGameObject(tex, x, y),
                                                               m_Direction(NONE),
                                                               m_OldDir(NONE),
                                                               m_Speed(0),
                                                               m_Stuck(false),
                                                               m_X(x),
                                                               m_Y(y),
                                                               m_PosX(x),
                                                               m_PosY(y),
                                                               TEMPX(-1),
                                                               TEMPY(-1),
                                                               m_ChangableBg(nullptr)
{}

void CMovingObject::Move()
{
    switch (m_Direction)
    {
        case LEFT:
            m_PosX -= m_Speed;
            m_DestRect.x=(int)m_PosX;
            break;
        case RIGHT:
            m_PosX += m_Speed;
            m_DestRect.x=(int)m_PosX;
            break;
        case UP:
            m_PosY -= m_Speed;
            m_DestRect.y=(int)m_PosY;
            break;
        case DOWN:
            m_PosY += m_Speed;
            m_DestRect.y=(int)m_PosY;
            break;
        default:
            break;
    }
    m_Collider.x = m_DestRect.x + m_Collider.w/2;
    m_Collider.y = m_DestRect.y + m_Collider.h/2;
}

void CMovingObject::CheckCollisions()
{
    if( ((m_DestRect.x < 0) ||
         (m_DestRect.y < m_DestRect.w * 2) ||
         (m_DestRect.y > (CObjectManager::m_MapH + 1) * m_DestRect.w) ||
         (m_DestRect.x > (CObjectManager::m_MapW - 1) * m_DestRect.w)))
    { Back(); }
    else
    {
        int curCell = ((m_DestRect.x + CObject::GetCellSize()/2) / CObject::GetCellSize()) + CObjectManager::m_MapW*((m_DestRect.y + CObject::GetCellSize()/2) / CObject::GetCellSize()) ;
        Colliding(CObjectManager::m_ObjectMap[curCell]);
        switch (m_Direction)
        {
            case RIGHT:
                Colliding(CObjectManager::m_ObjectMap[curCell + 1]);
                Colliding(CObjectManager::m_ObjectMap[curCell - CObjectManager::m_MapW + 1]);
                Colliding(CObjectManager::m_ObjectMap[curCell + CObjectManager::m_MapW + 1]);
                break;
            case LEFT:
                Colliding(CObjectManager::m_ObjectMap[curCell - 1]);
                Colliding(CObjectManager::m_ObjectMap[curCell - CObjectManager::m_MapW - 1]);
                Colliding(CObjectManager::m_ObjectMap[curCell + CObjectManager::m_MapW - 1]);
            case UP:
                Colliding(CObjectManager::m_ObjectMap[curCell - CObjectManager::m_MapW]);
                Colliding(CObjectManager::m_ObjectMap[curCell - CObjectManager::m_MapW - 1]);
                Colliding(CObjectManager::m_ObjectMap[curCell - CObjectManager::m_MapW + 1]);
            case DOWN:
                Colliding(CObjectManager::m_ObjectMap[curCell + CObjectManager::m_MapW]);
                Colliding(CObjectManager::m_ObjectMap[curCell + CObjectManager::m_MapW + 1]);
                Colliding(CObjectManager::m_ObjectMap[curCell + CObjectManager::m_MapW - 1]);
                break;
            default:
                break;
        }
    }
}

void CMovingObject::Colliding(CGameObject * obj)
{
    SDL_Rect f = m_DestRect;
    SDL_Rect s = obj->GetCollider();

    int f_Left= f.x;
    int f_Right = f.x + f.w;
    int f_Top = f.y;
    int f_Bottom = f.y + f.h;

    int s_Left = s.x;
    int s_Right = s.x + s.w;
    int s_Top = s.y;
    int s_Bottom = s.y + s.h;

    if( f_Bottom > s_Top &&
        f_Top < s_Bottom &&
        f_Right > s_Left &&
        f_Left < s_Right )
    { HandleCollision(obj); }
}

void CMovingObject::Back()
{
    m_Speed*=-1;
    Move();
    m_Speed*=-1;
    m_Stuck = true;
}

void CMovingObject::HandleCollision(CGameObject *object)
{ object->Collides(this); }

void CMovingObject::DefaultPos()
{
    SetPos(m_X, m_Y);
    int curCell = ((m_X + 1) / CObject::GetCellSize()) + CObjectManager::m_MapW*((m_Y +1) / CObject::GetCellSize());
    if(CObjectManager::m_ObjectMap[curCell-1]){m_Direction=LEFT;}
    else if(CObjectManager::m_ObjectMap[curCell+1]){m_Direction=RIGHT;}
    else if(CObjectManager::m_ObjectMap[curCell-28]){m_Direction=UP;}
    else if(CObjectManager::m_ObjectMap[curCell+28]){m_Direction=DOWN;}
}

void CMovingObject::SetPos(double x, double y)
{
    if(m_ChangableBg!=nullptr)
    { CGraphicsManager::GetInstance().Render(m_ChangableBg,m_DestRect,m_DestRect); }

    m_DestRect.x = (int)x;
    m_DestRect.y = (int)y;
    m_Collider.x = (int)x + m_Collider.w/2;
    m_Collider.y = (int)y + m_Collider.h/2;
    m_PosX = x;
    m_PosY = y;
}

void CMovingObject::SetChangableBg(SDL_Texture *tex)
{ m_ChangableBg = tex; }

void CMovingObject::SetSpeed(double speed)
{ m_Speed = speed; }