#pragma once
#include <SDL2/SDL.h>
#include "CObject.h"

class CHealth : public CObject
{
public:
    CHealth();
    void Draw() override;
    int GetLifes() const;

    /*! Sets texure of one life in health range */
    void SetTex(SDL_Texture * tex);

    /*! Decrement current lifes and change erase one life from health range */
    void operator --();

    void SetLifes(int lifes);
    /*! Sets current lifes to default value */
    void SetDefault();
private:
    /*! Current lifes && default(set in constructor) lifes */
    int m_CurrentLifes;
    int m_DefaultLifes;
};