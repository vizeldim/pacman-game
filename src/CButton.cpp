#include <stdexcept>
#include "CButton.h"
#include "CGraphicsManager.h"
#include "CInputManager.h"

using namespace std;

CButton::CButton(const string &    text,
                 const SDL_Rect &  rect,
                 ESTATE            nextState,
                 const SDL_Color & bg,
                 int               fontSize   ) : m_Content(nullptr),
                                                  m_Dest(rect),
                                                  m_NextState(nextState),
                                                  m_Clicked(false),
                                                  m_UpBtn(false)
{
    CGraphicsManager::GetInstance().CreateDrawTexture(m_Content,m_Dest.w,m_Dest.h);
    CGraphicsManager::GetInstance().FillTexture(m_Content,bg,255);

    SDL_Rect FontRect
    {
        10,
        rect.h/2 - fontSize/2,
        fontSize,
        fontSize
    };

    CGraphicsManager::GetInstance().DrawText(m_Content,text,FontRect);
    SDL_SetTextureBlendMode(m_Content,SDL_BLENDMODE_BLEND);
}

CButton::~CButton()
{ SDL_DestroyTexture(m_Content); }

void CButton::Show()
{
    if(CInputManager::GetInstance().IsMouseInArea(m_Dest))
    {
        MouseHover();
        DetectClick();
    }
    else
    { ResetClick(); }

    CGraphicsManager::GetInstance().Render(m_Content,m_Dest);
}

ESTATE CButton::GetNextState()
{
    ResetClick();
    return m_NextState;
}

void CButton::DetectClick()
{
    if(!m_Clicked)
    { m_Clicked = (CInputManager::GetInstance().e.type == SDL_MOUSEBUTTONDOWN); }
    else
    { m_UpBtn = (CInputManager::GetInstance().e.type == SDL_MOUSEBUTTONUP);}
}

void CButton::ResetClick()
{
    SDL_SetTextureAlphaMod(m_Content,255);
    m_UpBtn = false;
    m_Clicked = false;
}

bool CButton::IsClicked() const
{ return m_Clicked && m_UpBtn; }

void CButton::MouseHover()
{ SDL_SetTextureAlphaMod(m_Content,177); }