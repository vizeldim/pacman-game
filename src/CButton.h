#pragma once
#include "SDL2/SDL.h"
#include "ESTATE.h"
#include <string>

class CButton
{
public:
    CButton(const std::string & text,
            const SDL_Rect &    rect,
            ESTATE              nextState,
            const SDL_Color &   bg,
            int                 fontSize   );
    ~CButton();
    /*! Render button */
    void Show();
    /*! Returns TRUE if button was clicked */
    bool IsClicked() const;
    /*! Returns state to which is button supposed to switch */
    ESTATE GetNextState();
private:
    /*! Sets m_Clicked and m_UpButton to false */
    void ResetClick();

    /*! Checks if mouse is in button m_Dest rectangle area
     * -> if yes -> waits for click -> if clicked waits for mouse button up in the same area
     */

    void DetectClick();
    /*! Actions when mouse hovers button
     * -> Change button appearance -> make it transparent
     */
    void MouseHover();

    /*! Values which describe the button */
    SDL_Texture * m_Content;
    SDL_Rect m_Dest;
    ESTATE m_NextState;
    bool m_Clicked;
    bool m_UpBtn;
};
