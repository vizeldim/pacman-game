#pragma once
#include "CObject.h"

class CMovingObject;
class CPlayer;
class CGameObject : public CObject
{
public:
    CGameObject();
    CGameObject(SDL_Texture * tex, int x, int y);
    CGameObject(int x, int y);

    virtual void Collides(CMovingObject *p){};
    virtual void Collides(CPlayer *p){};

    SDL_Point GetPos();
    int GetX();
    int GetY();

    SDL_Rect & GetCollider();
    bool IsAccessable() const;

    bool m_CrossWay;
protected:
    bool m_Accessable;
    SDL_Rect m_Collider{};
};