#include "CModeScatter.h"
#include "CGhost.h"

void CModeScatter::StartMode()
{ m_Timer.Start(); }

void CModeScatter::CheckSwitch()
{
    if(m_Timer.GetTime() > m_Duration)
    { SwitchMode(CHASE); }
}

void CModeScatter::CalculateTarget()
{ m_Target = m_Ghost -> m_Corner; }