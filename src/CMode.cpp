#include <iostream>
#include "CMode.h"
#include "CGhost.h"
#include "CObjectManager.h"
#include "CPlayer.h"
#include "GameFuncs.h"

using namespace std;

CMode::CMode() : m_Duration(-1),
                 m_Ghost(nullptr),
                 m_Color{255,255,255},
                 m_Target{0,0}
{}

void CMode::SetGhost(CGhost *g)
{ m_Ghost = g; }

void CMode::StartMode()
{
    m_Ghost->ModeChanged();
    m_Timer.Start();
}

void CMode::CheckSwitch()
{
    if(m_Timer.GetTime() > m_Duration)
    { SwitchMode(CHASE); }
}

void CMode::SwitchMode(EMODE mode)
{
    EndMode();
    m_Ghost->SetMode(mode);
}

void CMode::UseColor(SDL_Texture * tex)
{ SDL_SetTextureColorMod(tex,m_Color.r,m_Color.g,m_Color.b); }

void CMode::SetColor(const SDL_Color &color)
{ m_Color = color; }

void CMode::SetDuration(unsigned int duration)
{ m_Duration = duration; }

void CMode::Move()
{
    int curCellA = ((m_Ghost->GetX() + 1) / CObject::GetCellSize()) + CObjectManager::m_MapW*((m_Ghost->GetY() +1) / CObject::GetCellSize()) ;
    //cout << m_Timer.GetTime() << endl;
    if(CObjectManager::m_ObjectMap[curCellA]->m_CrossWay)
    {
        CalculateTarget();
        DIR shrt = NONE;
        double distance = 1000000;
        double newDistance;
        DIR oposite = GameFuncs::OpositeDir(m_Ghost->m_Direction);
        for(int i = 0; i < 4; ++i)
        {
            SDL_Point pos;
            bool acc;
            GetNextX(pos,(DIR)i,acc, curCellA);

            if(acc && oposite!=(DIR)i)
            {
                newDistance = GameFuncs::GetDistance(pos, m_Target);
                if(distance > newDistance )
                {
                    distance = newDistance;
                    shrt = (DIR)i;
                }
            }
        }
        m_Ghost -> m_Direction = shrt;
    }
}

void CMode::GetNextX(SDL_Point & pos, DIR dir, bool &acc, int curc)
{
    int offset = 0;
    pos = {m_Ghost->GetX(), m_Ghost->GetY()};
    switch(dir)
    {
        case LEFT:
            offset = - 1;
            pos.x -= m_Ghost->m_DestRect.w;
            break;
        case RIGHT:
            offset = + 1;
            pos.x += m_Ghost->m_DestRect.w;
            break;
        case UP:
            offset = - CObjectManager::m_MapW;
            pos.y -= m_Ghost->m_DestRect.w;
            break;
        case DOWN:
            offset = + CObjectManager::m_MapW;
            pos.y += m_Ghost->m_DestRect.w;
            break;
        default:
            break;
    }
    if( ((curc + offset) >= 0) && (curc+offset < (int)CObjectManager::m_ObjectMap.size()))
    { acc = CObjectManager::m_ObjectMap[curc+offset]->IsAccessable(); }
}

void CMode::Pause()
{ m_Timer.Pause(); }

void CMode::UnPause()
{ m_Timer.Continue(); }
