#pragma once
#include "CModeChase.h"

class CModeScatter : public CModeChase
{
public:
    void StartMode() override;
    void CheckSwitch() override;
    void EndMode() override{};
    void CalculateTarget() override;
};