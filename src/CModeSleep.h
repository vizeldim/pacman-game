#pragma once
#include "CModeHouse.h"

class CModeSleep : public CModeHouse
{
public:
    void CheckSwitch() override;
    void SetDuration(unsigned int duration) override;
    int FoodAmount;
};

