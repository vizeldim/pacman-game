CXX=g++
CXXFLAGS=-Wall -pedantic
LD=g++
LDFLAGS=-Wall -pedantic
EXEC = ./vizeldim
OBJ=CApplication.o CButton.o CCountDown.o CEndGame.o CFood.o CGame.o CGameObject.o CGameRestarter.o CGhost.o CGraphicsManager.o CHealth.o CInputManager.o CMenu.o CModeChaseB.o CModeChaseC.o CModeChaseDistance.o CModeChaseD.o CModeChase.o CModeChaseRandom.o CModeFrightened.o CModeHouse.o CMode.o CModeScatter.o CModeSleep.o CMovingObject.o CObjectManager.o CObject.o COverlayTitle.o CPlayer.o CPowerFood.o CScore.o CStateManager.o CState.o CTeleport.o CTimer.o CWall.o main.o GameFuncs.o CContentMenu.o

all:
	make compile
	make doc


$(EXEC): $(OBJ)
	$(LD) $(LDFLAGS) $(OBJ) -lSDL2 -o $(EXEC)

compile: $(EXEC)

run:
	./vizeldim

clean:
	-rm *.o
	-rm $(EXEC)
	-rm -rf doc
	-rm scores.txt
doc:
	-mkdir doc
	doxygen Doxyfile
	
	
%.o: src/%.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@
	
CApplication.o: src/CApplication.cpp src/CApplication.h \
 src/CStateManager.h src/ESTATE.h src/CTimer.h src/CMenu.h src/CState.h \
 src/CGame.h src/CObjectManager.h src/CObject.h src/CScore.h \
 src/EGhostType.h src/EGameTitle.h src/CCountDown.h src/CGameRestarter.h \
 src/CEndGame.h src/CContentMenu.h src/ComponentSettings.h \
 src/CGraphicsManager.h src/CInputManager.h src/EDir.h
CButton.o: src/CButton.cpp src/CButton.h src/ESTATE.h \
 src/CGraphicsManager.h src/CInputManager.h src/EDir.h
CContentMenu.o: src/CContentMenu.cpp src/CContentMenu.h src/CMenu.h \
 src/CState.h src/ESTATE.h src/CTimer.h src/CGraphicsManager.h \
 src/AppSettings.h
CCountDown.o: src/CCountDown.cpp src/CCountDown.h src/CTimer.h \
 src/CGraphicsManager.h src/CInputManager.h src/EDir.h src/ESTATE.h \
 src/AppSettings.h
CEndGame.o: src/CEndGame.cpp src/CEndGame.h src/CState.h src/ESTATE.h \
 src/CTimer.h src/CInputManager.h src/EDir.h
CFood.o: src/CFood.cpp src/CFood.h src/CGameObject.h src/CObject.h \
 src/CPlayer.h src/EDir.h src/CMovingObject.h src/CScore.h src/CHealth.h \
 src/CObjectManager.h src/EGhostType.h
CGame.o: src/CGame.cpp src/CGame.h src/CObjectManager.h src/CObject.h \
 src/CScore.h src/EGhostType.h src/CState.h src/ESTATE.h src/CTimer.h \
 src/EGameTitle.h src/CCountDown.h src/CInputManager.h src/EDir.h \
 src/CGraphicsManager.h src/COverlayTitle.h src/CPlayer.h \
 src/CMovingObject.h src/CGameObject.h src/CHealth.h
CGameObject.o: src/CGameObject.cpp src/CGameObject.h src/CObject.h
CGameRestarter.o: src/CGameRestarter.cpp src/CGameRestarter.h \
 src/CState.h src/ESTATE.h src/CTimer.h src/CGame.h src/CObjectManager.h \
 src/CObject.h src/CScore.h src/EGhostType.h src/EGameTitle.h \
 src/CCountDown.h
CGhost.o: src/CGhost.cpp src/CGhost.h src/CMovingObject.h src/EDir.h \
 src/CGameObject.h src/CObject.h src/EMODE.h src/CObjectManager.h \
 src/CScore.h src/EGhostType.h src/CPlayer.h src/CHealth.h src/CMode.h \
 src/CTimer.h src/CModeChase.h src/CModeFrightened.h \
 src/CModeChaseRandom.h src/CModeHouse.h src/CModeSleep.h \
 src/CModeScatter.h src/CGraphicsManager.h src/CModeChaseDistance.h
CGraphicsManager.o: src/CGraphicsManager.cpp src/CGraphicsManager.h \
 src/CGameObject.h src/CObject.h src/AppSettings.h
CHealth.o: src/CHealth.cpp src/CHealth.h src/CObject.h \
 src/CGraphicsManager.h
CInputManager.o: src/CInputManager.cpp src/CInputManager.h src/EDir.h \
 src/ESTATE.h
CMenu.o: src/CMenu.cpp src/CMenu.h src/CState.h src/ESTATE.h src/CTimer.h \
 src/CGraphicsManager.h src/CButton.h src/CApplication.h \
 src/CStateManager.h src/AppSettings.h
CModeChaseB.o: src/CModeChaseB.cpp src/CModeChaseB.h src/CModeChase.h \
 src/CMode.h src/CTimer.h src/EMODE.h src/EDir.h src/CPlayer.h \
 src/CMovingObject.h src/CGameObject.h src/CObject.h src/CScore.h \
 src/CHealth.h src/CGhost.h
CModeChaseC.o: src/CModeChaseC.cpp src/CModeChaseC.h src/CModeChase.h \
 src/CMode.h src/CTimer.h src/EMODE.h src/EDir.h src/CGhost.h \
 src/CMovingObject.h src/CGameObject.h src/CObject.h src/CPlayer.h \
 src/CScore.h src/CHealth.h src/GameFuncs.h
CModeChase.o: src/CModeChase.cpp src/CModeChase.h src/CMode.h \
 src/CTimer.h src/EMODE.h src/EDir.h src/CObjectManager.h src/CObject.h \
 src/CScore.h src/EGhostType.h src/CGhost.h src/CMovingObject.h \
 src/CGameObject.h src/CPlayer.h src/CHealth.h
CModeChaseD.o: src/CModeChaseD.cpp src/CModeChaseD.h src/CModeChaseB.h \
 src/CModeChase.h src/CMode.h src/CTimer.h src/EMODE.h src/EDir.h \
 src/CGhost.h src/CMovingObject.h src/CGameObject.h src/CObject.h \
 src/CPlayer.h src/CScore.h src/CHealth.h
CModeChaseDistance.o: src/CModeChaseDistance.cpp src/CModeChaseDistance.h \
 src/CModeChase.h src/CMode.h src/CTimer.h src/EMODE.h src/EDir.h \
 src/CGhost.h src/CMovingObject.h src/CGameObject.h src/CObject.h \
 src/CPlayer.h src/CScore.h src/CHealth.h src/GameFuncs.h
CModeChaseRandom.o: src/CModeChaseRandom.cpp src/CModeChaseRandom.h \
 src/CModeChase.h src/CMode.h src/CTimer.h src/EMODE.h src/EDir.h
CMode.o: src/CMode.cpp src/CMode.h src/CTimer.h src/EMODE.h src/EDir.h \
 src/CGhost.h src/CMovingObject.h src/CGameObject.h src/CObject.h \
 src/CObjectManager.h src/CScore.h src/EGhostType.h src/CPlayer.h \
 src/CHealth.h src/GameFuncs.h
CModeFrightened.o: src/CModeFrightened.cpp src/CModeFrightened.h \
 src/CModeChaseRandom.h src/CModeChase.h src/CMode.h src/CTimer.h \
 src/EMODE.h src/EDir.h src/CGhost.h src/CMovingObject.h \
 src/CGameObject.h src/CObject.h src/CPlayer.h src/CScore.h src/CHealth.h
CModeHouse.o: src/CModeHouse.cpp src/CModeHouse.h src/CMode.h \
 src/CTimer.h src/EMODE.h src/EDir.h src/CGhost.h src/CMovingObject.h \
 src/CGameObject.h src/CObject.h
CModeScatter.o: src/CModeScatter.cpp src/CModeScatter.h src/CModeChase.h \
 src/CMode.h src/CTimer.h src/EMODE.h src/EDir.h src/CGhost.h \
 src/CMovingObject.h src/CGameObject.h src/CObject.h
CModeSleep.o: src/CModeSleep.cpp src/CModeSleep.h src/CModeHouse.h \
 src/CMode.h src/CTimer.h src/EMODE.h src/EDir.h src/CPlayer.h \
 src/CMovingObject.h src/CGameObject.h src/CObject.h src/CScore.h \
 src/CHealth.h src/CGhost.h
CMovingObject.o: src/CMovingObject.cpp src/CMovingObject.h src/EDir.h \
 src/CGameObject.h src/CObject.h src/CGraphicsManager.h \
 src/CObjectManager.h src/CScore.h src/EGhostType.h
CObject.o: src/CObject.cpp src/CObject.h src/CGraphicsManager.h
CObjectManager.o: src/CObjectManager.cpp src/CObjectManager.h \
 src/CObject.h src/CScore.h src/EGhostType.h src/CPlayer.h src/EDir.h \
 src/CMovingObject.h src/CGameObject.h src/CHealth.h src/CWall.h \
 src/CTeleport.h src/CPowerFood.h src/CFood.h src/CGraphicsManager.h \
 src/CModeChase.h src/CMode.h src/CTimer.h src/EMODE.h src/CModeChaseB.h \
 src/CModeChaseC.h src/CModeChaseD.h src/CGhost.h src/ObjectSettings.h \
 src/AppSettings.h
COverlayTitle.o: src/COverlayTitle.cpp src/COverlayTitle.h src/CTimer.h \
 src/AppSettings.h src/CGraphicsManager.h
CPlayer.o: src/CPlayer.cpp src/CPlayer.h src/EDir.h src/CMovingObject.h \
 src/CGameObject.h src/CObject.h src/CScore.h src/CHealth.h \
 src/CInputManager.h src/ESTATE.h src/CGraphicsManager.h src/CGhost.h \
 src/EMODE.h
CPowerFood.o: src/CPowerFood.cpp src/CPowerFood.h src/CFood.h \
 src/CGameObject.h src/CObject.h src/CPlayer.h src/EDir.h \
 src/CMovingObject.h src/CScore.h src/CHealth.h
CScore.o: src/CScore.cpp src/CScore.h src/CObject.h \
 src/CGraphicsManager.h
CState.o: src/CState.cpp src/CState.h src/ESTATE.h src/CTimer.h \
 src/CGraphicsManager.h
CStateManager.o: src/CStateManager.cpp src/CStateManager.h src/ESTATE.h \
 src/CTimer.h src/CState.h src/CInputManager.h src/EDir.h
CTeleport.o: src/CTeleport.cpp src/CTeleport.h src/CGameObject.h \
 src/CObject.h src/EDir.h src/CMovingObject.h
CTimer.o: src/CTimer.cpp src/CTimer.h
CWall.o: src/CWall.cpp src/CWall.h src/CGameObject.h src/CObject.h \
 src/CPlayer.h src/EDir.h src/CMovingObject.h src/CScore.h src/CHealth.h
GameFuncs.o: src/GameFuncs.cpp src/GameFuncs.h src/EDir.h
main.o: src/main.cpp src/CApplication.h src/CStateManager.h src/ESTATE.h \
 src/CTimer.h
