#pragma once
namespace AppSettings
{
    const int       GAME_FPS            = 120;

    /*! GAME TITLES */
    const char *    GAME_OVER_TITLE     = "GAME OVER";
    const char *    NEXT_LEVEL_TITLE    = "NEXT LEVEL";
    const char *    WIN_TITLE           = "YOU WON";

    /*! CONFIG SOURCES */
    const char *    GAME_LEVELS_FILE    = "examples/levels.txt";
    const char *    PLAYER_SCORES_FILE  = "scores.txt";

    /*! BUTTON TITLES */
    const char *          BTN_PLAY_TITLE      = "PLAY";
    const char *          BTN_SCORES_TITLE    = "SCORES";
    const char *          BTN_CLOSE_TITLE     = "CLOSE";
    const char *          BTN_CONTINUE_TITLE  = "CONTINUE";
    const char *          BTN_QUIT_TITLE      = "QUIT GAME";
    const char *          BTN_BACK_TITLE      = "BACK";

    /*! BTN */
    const int       BTN_FONT_SIZE       = 13;
    const int       BTN_WIDTH           = 180;
    const int       BTN_HEIGHT          = 50;
    const int       BTN_MARGIN          = 30;
    const SDL_Color BTN_COLOR           = {255,255,255};

    /*! FONT -> CHARS && NUMS */
    const char *          FONT_IMG            = "examples/design/letters.bmp";
    const int             SRC_CHAR_SIZE       = 5;
    const unsigned int    LETTER_SPACING      = 5;

    const char *          NUMS_IMG            = "examples/design/numbers.bmp";
    const int             SRC_NUM_WIDTH       = 4;
    const int             SRC_NUM_HEIGHT      = 5;

    const SDL_Color       FONT_COLOR          = {0,255,0};

    /*! MENU DESIGN */
    const SDL_Color        BG_COLOR            = {0,0,0};
    const int              LAYER_OPACITY       = 200;
}
