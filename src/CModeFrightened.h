#pragma once
#include "CModeChaseRandom.h"

class CModeFrightened : public CModeChaseRandom
{
    void EndMode() override;
    void StartMode() override;
    void CheckSwitch() override;
    void Move() override;
    void Collide(CPlayer *) override;
};


