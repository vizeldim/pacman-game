#include "CTimer.h"
#include "SDL2/SDL.h"

void CTimer::Start()
{ m_StartTime = SDL_GetTicks(); }

unsigned int CTimer::GetTime() const
{ return SDL_GetTicks() - m_StartTime; }

void CTimer::Pause()
{ m_PauseStartTime = SDL_GetTicks() - m_StartTime; }

void CTimer::Continue()
{ m_StartTime = SDL_GetTicks() - m_PauseStartTime; }

void CTimer::Wait(unsigned int duration)
{
    if(GetTime() < duration)
    { SDL_Delay(duration - GetTime()); }
}