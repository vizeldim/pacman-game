#include "CModeHouse.h"
#include "CGhost.h"

void CModeHouse::Move()
{
    if(m_Ghost->m_Direction == UP )
    {m_Ghost->m_Direction = DOWN;}
    else { m_Ghost->m_Direction = UP; };
}

void CModeHouse::SwitchMode(EMODE mode)
{
    if(mode==CHASE)
    { CMode::SwitchMode(mode); }
}

void CModeHouse::EndMode()
{ m_Ghost->GetOut(); }

void CModeHouse::StartMode()
{
    m_Ghost->DefaultPos();
    CMode::StartMode();
}
