#pragma once
#include "CTimer.h"

class CCountDown
{
public:
    explicit CCountDown(unsigned int from = 3,
                        unsigned int interval = 700);
    void Run();
private:
    /*! First number from which CoundDown start */
    unsigned int m_CountFrom;
    /*! Interval between showing number and decrementing counter (in MILLISECONDS)*/
    unsigned int m_Interval;
    CTimer m_Timer{};
};