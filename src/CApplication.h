#pragma once
#include "CStateManager.h"

class CApplication
{
public:
    /*! Calls configure -> if no exception thrown -> runs current state */
    int Run();
private:
    /*! Allocate all states + configure them */
    void Configure();

    /*! Controls switching between states */
    CStateManager m_StateManager;
};