#include "CModeChaseD.h"
#include "CGhost.h"
#include "CPlayer.h"

CModeChaseD::CModeChaseD(CGhost *g) : m_Helper(g)
{}

void CModeChaseD::CalculateTarget()
{
    m_Target = m_Ghost->m_TargetPlayer->GetPos();
    ShiftTarget(2);

    if(m_Helper->m_CurrentMode!=SLEEP && m_Helper->m_CurrentMode!=HOME)
    {
        m_Target.x += (m_Target.x - m_Helper->GetX()) * 2;
        m_Target.y += (m_Target.y - m_Helper->GetY()) * 2;
    }
}

