#include <iostream>
#include "CStateManager.h"
#include "CState.h"
#include "CInputManager.h"

using namespace std;

CStateManager::CStateManager() : m_FPS(100),
                                 m_CurrentState(GAME)
{}

CStateManager::~CStateManager()
{
    for(auto s : m_States)
    { delete s.second; }
}

bool CStateManager::RunCurrentState()
{
    m_States[m_CurrentState] -> PrepareState();
    CInputManager::GetInstance().Reset();
    int FrameDuration = 1000/m_FPS;
    while (true)
    {
        m_Timer.Start();

        CInputManager::GetInstance().ReadInput();

        m_States[m_CurrentState] -> StartStateFrame();
        m_States[m_CurrentState] -> RunState();
        m_States[m_CurrentState] -> EndStateFrame();

        if(CInputManager::GetInstance().m_Quit)
        { return false; }

        if(m_States[m_CurrentState] -> ChangeState(m_NextState))
        {
            SetState(m_NextState);
            break;
        }
        m_Timer.Wait(FrameDuration);
    }
    return true;
}

void CStateManager::SetFPS(const int fps)
{ m_FPS = fps; }

void CStateManager::AddState(ESTATE stateName, CState *state)
{ m_States.insert(make_pair(stateName,state)); }

void CStateManager::SetState(ESTATE state)
{
    if( m_States.find(state) == m_States.end() )
    { cout << "Some State Not Found." << endl;}
    else if(m_States[state] == nullptr)
    { cout << "Some State was Deleted." << endl; }
    else
    { m_CurrentState = state; }
}

void CStateManager::CheckStates()
{
    if(m_States.find(GAME) == m_States.end() || m_States[GAME] == nullptr)
    { throw std::runtime_error("Game state was not initialised"); }
}
