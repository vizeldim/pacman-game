#pragma once
#include <SDL2/SDL.h>
#include <vector>
#include <string>
#include "CState.h"

class CButton;
class CGame;

class CMenu : public CState
{
public:
    CMenu( const SDL_Color & bgColor,
            int               opacity);

    ~CMenu() override;
    void RunState() override;
    bool ChangeState(ESTATE &state) override;

    void AddButton(const std::string& text, ESTATE nextState);
    void SetGameBackground(SDL_Texture * tex);

    void SetButtonStyle(int btnW, int btnH, int btnMargin, int fontSize, const SDL_Color & color);
private:
    /*! Transparent layer texture */
    SDL_Texture * m_Content;

    /*! Background texture */
    SDL_Texture * m_GameBackground;

    /*! Container to store all buttons */
    std::vector<CButton*> m_Buttons;

    int m_BtnW, m_BtnH;
    int m_BtnMargin;
    int m_BtnFonstSize;
    SDL_Color m_BtnBgColor;
};