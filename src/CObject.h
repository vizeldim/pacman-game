#pragma once
#include <SDL2/SDL.h>

class CObject
{
public:
    CObject();
    CObject(SDL_Texture * tex, int x, int y);
    CObject(int x, int y);
    virtual ~CObject() = default;

    virtual void Draw();

    SDL_Rect m_DestRect{};

    static int GetCellSize();
protected:
    SDL_Texture * m_Texture;
private:
    static int m_CellSize;
};