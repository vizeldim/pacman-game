#pragma once

enum EMODE
{
    SLEEP = 0,
    HOME = 1,
    FRIGHTENED = 2,
    SCATTER = 3,
    CHASE = 4
};