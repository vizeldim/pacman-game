#pragma once
#include <SDL2/SDL.h>
#include "EDir.h"

namespace GameFuncs
{
    double GetDistance(const SDL_Point &first, const SDL_Point & second);
    DIR OpositeDir(DIR d);
}