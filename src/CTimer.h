#pragma once
class CTimer
{
public:
    void Start();
    void Pause();
    void Continue();
    void Wait(unsigned int duration);
    /*! Returns time in miliseconds since timer has been started */
    unsigned int GetTime() const;
private:
    unsigned int m_StartTime;
    unsigned int m_PauseStartTime;
};