#pragma once
#include <map>
#include <vector>
#include "CMovingObject.h"
#include "EMODE.h"

class CMode;
class CModeChase;

class CGhost : public CMovingObject
{
public:
    CGhost(SDL_Texture * tex, int x, int y, CModeChase * mode);
    ~CGhost() override;
    
    void Update() override;
    void Draw() override;

    /*! @parameter frightenColor -> sets ghosts color in frighten mode
     *  @parameter mainColor -> sets ghosts color in other modes
     */
    void SetColor(const SDL_Color & mainColor, const SDL_Color & frightenColor);

    void ChangeDir();

    void Collides(CPlayer * p) override;

    /*! Sets mode and starts it */
    void SetMode(const EMODE& mode);
    /*! Asks current mode if can change to another mode send as @parameter (m) */
    void ChangeMode(EMODE m);
    EMODE m_CurrentMode;

    /*! Ghosts main target == player == pacman */
    CPlayer * m_TargetPlayer;

    /*! Slow down 2x */
    void Slow();

    /*! Speed up 2x */
    void Fast();

    /*! Move in oposite direction */
    void RunAway();

    /*! Get out of ghost house */
    void GetOut();

    void Configure(const int modeChangers[4]);

    void SetScatter(const int scatterTimes[10]);

    void ModeChanged();

    /*! @Parameter[out] sets next value of ghosts scatter mode duration
     *  @Returns true if next Scatter Time exists.
     * */
    bool ChangeDuration(unsigned int &duration);

    /*! Pause && Unpause ghosts current mode timer */
    void Pause();
    void UnPause();

    SDL_Point m_Corner;
    SDL_Point m_HouseDoor;
private:
    /*! Container holds all Times when ghosts mode changes from CHASE to SCATTER */
    std::vector<unsigned int> m_ScatterTimes;
    /*! Holds index of Current Scatter Time in Container(m_ScatterTimes) */
    unsigned int m_CurrentDuration;

    void CheckChangeMode();

    std::map<EMODE,CMode *> m_Mode;

    void DrawBgPart();
};
