#pragma once
#include "CGameObject.h"

class CPlayer;
class CFood : public CGameObject
{
public:
    CFood(SDL_Texture * tex, int x, int y);
    void Draw() override;
    void Collides(CPlayer * p) override;
protected:
    /*! Detects if food has been eaten == TRUE */
    bool m_Eaten;

    /*! Handle collision with player
     *  -> food becomes eaten && increment players score
     */
    virtual void HandleCollision(CPlayer * p);
};