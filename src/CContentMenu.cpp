#include <fstream>
#include <iostream>
#include "CContentMenu.h"
#include "CGraphicsManager.h"
#include "AppSettings.h"

CContentMenu::CContentMenu( std::string     path,
                            const SDL_Color &bgColor,
                            int             opacity   ) : CMenu(bgColor, opacity),
                                                          m_FilePath(std::move(path))
{}

void CContentMenu::RunState()
{
    CMenu::RunState();
    ShowContent();
}

void CContentMenu::ShowContent()
{
    for(int i = 0; i < (int)m_Content.size(); ++i)
    { CGraphicsManager::GetInstance().DrawNumber(m_Content[i],SDL_Rect{AppSettings::WINDOW_WIDTH/2 + 30,120+i * 40,13,13}); }
}

void CContentMenu::PrepareState()
{
    std::ifstream inFile(m_FilePath);
    if (!(inFile.is_open()))
    { std::cout << "Cant open scores file -> Cant show scores or show old values" << std::endl; }
    else
    {
        int num;
        unsigned int i = 0;
        while (inFile >> num)
        {
            if (m_Content.size() > i)
            { if (m_Content[i] != num) { m_Content[i] = num; } }
            else
            { m_Content.push_back(num); }
            ++i;
        }
    }
}