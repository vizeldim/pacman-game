#include "CFood.h"
#include "CPlayer.h"
#include "CObjectManager.h"

CFood::CFood(SDL_Texture * tex, int x, int y) : CGameObject(tex, x, y),
                                                m_Eaten(false)
{
    m_Accessable = true;

    m_Collider.w = m_DestRect.w/5;
    m_Collider.h = m_DestRect.w/5;
    m_Collider.x = x + (m_DestRect.w - m_Collider.w)/2;
    m_Collider.y = y + (m_DestRect.w - m_Collider.w)/2;
}

void CFood::Collides(CPlayer *p)
{
    if(!m_Eaten)
    { HandleCollision(p); }
}

void CFood::Draw()
{
    if(!m_Eaten)
    { CObject::Draw(); }
}

void CFood::HandleCollision(CPlayer *p)
{
    p -> HideOnBg(this);
    p -> IncScore(1);
    p -> Eat();
    m_Eaten = true;
}