#pragma once
#include "CModeChase.h"

class CModeChaseB : public CModeChase
{
public:
    void CalculateTarget() override;
    void ShiftTarget(int cells);
};
