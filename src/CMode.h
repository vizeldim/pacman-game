#pragma once
#include <SDL2/SDL_pixels.h>
#include <SDL2/SDL_render.h>
#include "CTimer.h"
#include "EMODE.h"
#include "EDir.h"

class CGhost;
class CPlayer;

class CMode
{
public:
    CMode();
    virtual ~CMode() = default;
    
    /*! Sets Ghost which will be controlled */
    void SetGhost(CGhost * g);

    /*! Switch mode if not forbidden */
    virtual void SwitchMode(EMODE mode);

    /*! Check if its time to switch mode */
    virtual void CheckSwitch();

    /*! Ghost movement and collision handling in a mode */
    virtual void Move();
    virtual void Collide(CPlayer *) = 0;

    /*! Calculate new target */
    virtual void CalculateTarget() = 0;

    /*! start && end mode actions */
    virtual void StartMode();
    virtual void EndMode() = 0;

    /*! Set mode duration */
    virtual void SetDuration(unsigned int duration);

    /*! Sets Ghost color in to current mode color */
    void UseColor(SDL_Texture * tex);

    /*! Set mode color */
    void SetColor(const SDL_Color& color);

    /*! Duration of a mode */
    unsigned int m_Duration;

    /*! Pause && UnPause modes timer */
    void Pause();
    void UnPause();
protected:
    /*! GHOST controlled by mode && His COLOR in mode */
    CGhost * m_Ghost;
    SDL_Color m_Color;

    /*! Ghosts target point (x,y) */
    SDL_Point m_Target;

    /*! Timer for CheckSwitch */
    CTimer m_Timer{};

    void GetNextX(SDL_Point & pos, DIR dir, bool &acc, int curc);
};
