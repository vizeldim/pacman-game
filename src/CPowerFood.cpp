#include "CPowerFood.h"
#include "CPlayer.h"

CPowerFood::CPowerFood(SDL_Texture * tex, int x, int y) : CFood(tex, x, y)
{
    m_Collider.x = x + 3;
    m_Collider.y = y + 3;
    m_Collider.w = 14;
    m_Collider.h = 14;
}

void CPowerFood::HandleCollision(CPlayer *p)
{
    CFood::HandleCollision(p);
    p -> Undefeatable();
}
