#pragma once
#include <SDL2/SDL.h>
#include "EDir.h"
#include "ESTATE.h"

class CInputManager
{
public:
    static CInputManager & GetInstance();
	void Delete();
	
    void Reset();
    void ReadInput();
    bool IsMouseInArea(SDL_Rect const &rect);
    DIR GetDir();

    bool m_Pause;
    bool m_Quit;
    SDL_Event e{};
private:
    CInputManager();
    void ReadKey();
    static CInputManager * m_Instance;
    DIR m_Direction;
};
