#pragma once
#include "ESTATE.h"
#include "CTimer.h"

class CApplication;
class CState
{
public:
    virtual ~CState() = default;

    /*! Start state actions */
    virtual void PrepareState();

    /*! Start frame actions => Called in the beginning of each frame */
    virtual void StartStateFrame();

    /*! State actions */
    virtual void RunState() = 0;

    /*! End frame actions => Called in the end of each frame */
    virtual void EndStateFrame();

    /*! Check for state end Condition
     * -> end of state == returns TRUE
     * -> writes next state in out @parameter
     */
    virtual bool ChangeState(ESTATE &state) = 0;
protected:
    CTimer m_Timer{};
};