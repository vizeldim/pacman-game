#pragma once
#include <string>
#include <SDL2/SDL.h>
#include "CTimer.h"

class COverlayTitle
{
public:
    COverlayTitle( const SDL_Color &  bg,
                   int                opacity,
                   unsigned int time = 2000 );
    ~COverlayTitle();
    void Show();
    void Wait();
    void SetTitle(const std::string & text);
private:
    std::string m_Text;
    SDL_Texture * m_Content;
    CTimer m_Timer{};
    unsigned int m_ShowTime;
};
