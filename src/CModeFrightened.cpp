#include "CModeFrightened.h"
#include "CGhost.h"
#include "CPlayer.h"

void CModeFrightened::Collide(CPlayer * p)
{
    m_Ghost -> DefaultPos();
    m_Ghost -> ChangeMode(HOME);
    p->IncScore(100);
}

void CModeFrightened::Move()
{
    CModeChaseRandom::Move();
   // if(m_Timer.GetTime() >= m_Duration*0.8)
   // { m_Color.g = 125; }
}

void CModeFrightened::StartMode()
{
    CModeChaseRandom::StartMode();
    m_Ghost->RunAway();
    m_Ghost->Slow(); //SPEED
    m_Timer.Start();
}

void CModeFrightened::EndMode()
{ m_Ghost -> Fast(); }

void CModeFrightened::CheckSwitch()
{
    if(m_Timer.GetTime() > m_Duration)
    { SwitchMode(CHASE); }
}
