#include <iostream>
#include "CApplication.h"
#include "CMenu.h"
#include "CGame.h"
#include "CGameRestarter.h"
#include "CEndGame.h"
#include "CContentMenu.h"
#include "ComponentSettings.h"
#include "CGraphicsManager.h"
#include "CInputManager.h"

using namespace std;

int CApplication::Run()
{
    try
    {Configure();}
    catch(runtime_error &err)
    {
        cout << "ERROR: " << err.what() << endl;
        return 1;
    }

    m_StateManager.SetState(MENU);
    while(m_StateManager.RunCurrentState()){ }

	CInputManager::GetInstance().Delete();
    CGraphicsManager::GetInstance().Delete();
    return 0;
}

void CApplication::Configure()
{
	CInputManager::GetInstance();

    CGraphicsManager::GetInstance();
    CGraphicsManager::GetInstance().SetCharFont(AppSettings::FONT_IMG,
                                                  AppSettings::SRC_CHAR_SIZE,
                                                    AppSettings::LETTER_SPACING,
                                                  AppSettings::FONT_COLOR );

    CGraphicsManager::GetInstance().SetNumFont(AppSettings::NUMS_IMG,
                                                AppSettings::SRC_NUM_WIDTH,
                                                AppSettings::SRC_NUM_HEIGHT,
                                                AppSettings::FONT_COLOR );

    CGraphicsManager::GetInstance().SetDefaultColor(AppSettings::BG_COLOR);

    auto * game = new CGame();
    game -> LoadLevelInfo(AppSettings::GAME_LEVELS_FILE);
    game -> SetScoreFile(AppSettings::PLAYER_SCORES_FILE);
    game -> SetOverlayTitle(WIN, AppSettings::WIN_TITLE);
    game -> SetOverlayTitle(GAMEOVER, AppSettings::GAME_OVER_TITLE);
    game -> SetOverlayTitle(NEXTLEVEL, AppSettings::NEXT_LEVEL_TITLE);
    game -> ConfigureState();

    auto * menu = new CMenu(AppSettings::BG_COLOR, AppSettings::LAYER_OPACITY);
    menu -> SetButtonStyle(AppSettings::BTN_WIDTH,
                           AppSettings::BTN_HEIGHT,
                           AppSettings::BTN_MARGIN,
                           AppSettings::BTN_FONT_SIZE,
                           AppSettings::BTN_COLOR);

    menu -> SetGameBackground(game->GetBackground());
    menu -> AddButton(AppSettings::BTN_PLAY_TITLE       ,GAME    );
    menu -> AddButton(AppSettings::BTN_SCORES_TITLE     ,SCORES  );
    menu -> AddButton(AppSettings::BTN_CLOSE_TITLE      ,ENDGAME );

    auto * pause = new CMenu(AppSettings::BG_COLOR, AppSettings::LAYER_OPACITY);
    pause -> SetButtonStyle(AppSettings::BTN_WIDTH,
                            AppSettings::BTN_HEIGHT,
                            AppSettings::BTN_MARGIN,
                            AppSettings::BTN_FONT_SIZE,
                            AppSettings::BTN_COLOR);
    pause -> SetGameBackground(game->GetBackground());
    pause -> AddButton(AppSettings::BTN_CONTINUE_TITLE, GAME    );
    pause -> AddButton(AppSettings::BTN_QUIT_TITLE,     GAMESTART    );

    auto * scores = new CContentMenu(AppSettings::PLAYER_SCORES_FILE,
                                          AppSettings::BG_COLOR,
                                          AppSettings::LAYER_OPACITY);
    scores ->  SetButtonStyle(AppSettings::BTN_WIDTH,
                             AppSettings::BTN_HEIGHT,
                             AppSettings::BTN_MARGIN,
                             AppSettings::BTN_FONT_SIZE,
                             AppSettings::BTN_COLOR);
    scores -> SetGameBackground(game->GetBackground());
    scores -> AddButton(AppSettings::BTN_BACK_TITLE, MENU       );


    m_StateManager.AddState(ENDGAME,    new CEndGame()              );
    m_StateManager.AddState(GAMESTART,  new CGameRestarter(game)    );
    m_StateManager.AddState(GAME,       game                        );
    m_StateManager.AddState(SCORES,          scores                      );
    m_StateManager.AddState(MENU,            menu                        );
    m_StateManager.AddState(PAUSE,           pause                       );

    m_StateManager.CheckStates();
    m_StateManager.SetFPS(AppSettings::GAME_FPS);
}
