#pragma once
namespace AppSettings
{
    const double    MOVING_OBJECT_SPEED         = 1.25; //SPEEDS {1, 1.25, 2.5,4.5} + CHANGE FPS
    const int       DEFAULT_SCATTER_TIME[3]     = {20000, 80000, -1};
    const int       DEFAULT_GHOST_CONFIG[4][4]  = { {10, 5000,	 15000,	20000},
                                                    {30, 5000,	 5000,	40000},
                                                    {54, 7000,	 7000,	70000},
                                                    {4,  10000, 20000,	50000} };

    const int   PACMAN_HEALTH = 3;

    /*! GAME OBJECTS & DESIGN SOURCES */
    const char *    PACMAN_IMG          = "examples/design/pacman.bmp";
    const char *    PACMAN2_IMG         = "examples/design/pacman2.bmp";
    const char *    WALL_IMG            = "examples/design/wall.bmp";
    const char *    FOOD_IMG            = "examples/design/food.bmp";
    const char *    POWER_FOOD_IMG      = "examples/design/powerfood.bmp";
    const char *    GHOST_IMG           = "examples/design/ghost.bmp";

    /*! GHOST COLORS */
    const SDL_Color BLINKY_COLOR        = {255,0  ,0   };
    const SDL_Color PINKY_COLOR         = {255,184,255 };
    const SDL_Color INKY_COLOR          = {0  ,255,255 };
    const SDL_Color CLYDE_COLOR         = {255,184,82  };
    const SDL_Color FRIGHTENED_COLOR    = {255,255,0   };
}
