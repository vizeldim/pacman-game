#include "CModeChaseB.h"
#include "CPlayer.h"
#include "CGhost.h"

void CModeChaseB::CalculateTarget()
{
    CModeChase::CalculateTarget();
    ShiftTarget(4);
}

void CModeChaseB::ShiftTarget(int cells)
{
    int shift = cells * m_Ghost->m_DestRect.w;
    switch (m_Ghost->m_TargetPlayer->m_Direction)
    {
        case LEFT:
            m_Target.x -= shift;
            break;
        case RIGHT:
            m_Target.x += shift;
            break;
        case UP:
            m_Target.y -= shift;
            break;
        case DOWN:
            m_Target.y += shift;
            break;
        default:
            break;
    }
}