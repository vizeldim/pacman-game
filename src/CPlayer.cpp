#include "CPlayer.h"
#include "CInputManager.h"
#include "CGraphicsManager.h"
#include "CGhost.h"

using namespace std;

CPlayer::CPlayer(SDL_Texture *tex, int x, int y, int health) : CMovingObject(tex, x, y),
                                                               m_EatenAmount(0),
                                                               m_AnimationSpeed(5),
                                                               m_Frame(0),
                                                                m_CurrentAnimation(0)
{
    m_Animations.push_back(m_Texture);
    m_Health.SetLifes(health);
    m_Health.SetTex(m_Texture);
}

void CPlayer::IncScore(int i)
{ m_Score+=i; }

void CPlayer::Update()
{
    CGraphicsManager::GetInstance().DrawRect(m_DestRect);
    Animate();

    m_Direction = CInputManager::GetInstance().GetDir();
    Move();

   // if (TEMPX != m_DestRect.x || TEMPY != m_DestRect.y)
   // {
        CheckCollisions();
        if (!m_Stuck)
        {
            Rotate();
            m_OldDir = m_Direction;
        }
        else
        {
            m_Direction = m_OldDir;
            Move();
            CheckCollisions();
        }
        m_Stuck = false;
      //  TEMPX = m_DestRect.x;
      //  TEMPY = m_DestRect.y;
   // }
}

void CPlayer::Rotate()
{ m_Angle = 90 * m_Direction; }

void CPlayer::Draw()
{ CGraphicsManager::GetInstance().Render(m_Texture,m_DestRect,m_Angle); }

void CPlayer::HandleCollision(CGameObject *object)
{
    object->Collides(this);
    CMovingObject::HandleCollision(object);
}

void CPlayer::CheckCollisions()
{
    for(auto & m_Ghost : m_Enemies)
    { Colliding((CGameObject *) m_Ghost); }

    CMovingObject::CheckCollisions();
}

void CPlayer::DecHealth()
{ --m_Health; }

void CPlayer::DefaultPos()
{ SetPos(m_X, m_Y); }

int CPlayer::GetHealth()
{ return m_Health.GetLifes(); }

void CPlayer::SetPlayer(SDL_Texture *tex, int x, int y)
{
    m_Texture = tex;
    m_Animations.push_back(m_Texture);
    m_X = x;
    m_Y = y;
    SetPos(x,y);
}

int CPlayer::EatenAmount() const
{ return m_EatenAmount; }

void CPlayer::Eat()
{ ++m_EatenAmount;}

void CPlayer::AddAnimation(SDL_Texture * tex)
{ m_Animations.push_back(tex); }

void CPlayer::Animate()
{
    if (m_Frame == m_AnimationSpeed)
    {
        if (++m_CurrentAnimation >= (int)m_Animations.size())
        { m_CurrentAnimation = 0; }
        m_Texture = m_Animations[m_CurrentAnimation];
        m_Frame = 0;
    }
    m_Frame++;
}

void CPlayer::DrawSH()
{
    m_Score.Draw();
    m_Health.Draw();
}

void CPlayer::ResetInfo()
{
    m_EatenAmount = 0;
    m_Health.SetDefault();
    m_Enemies.clear();
}

void CPlayer::ResetScore()
{ m_Score.ResetScore(); }

void CPlayer::Undefeatable()
{
  for(CGhost * g : m_Enemies)
  { g -> ChangeMode(FRIGHTENED); }
}

int CPlayer::GetScore()
{ return m_Score.GetScore(); }

void CPlayer::SetSHposX(int scoreX, int healthX)
{
    m_Health.m_DestRect.x = healthX;
    m_Score.m_DestRect.x = scoreX;
}

void CPlayer::HideOnBg(CGameObject *obj)
{
    if(m_ChangableBg!=nullptr)
    { CGraphicsManager::GetInstance().HideOnTexture(m_ChangableBg,obj->GetCollider()); }
}