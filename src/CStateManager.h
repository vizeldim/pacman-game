#pragma once
#include <map>
#include "ESTATE.h"
#include "CTimer.h"

class CState;
class CStateManager
{
public:
    CStateManager();
    ~CStateManager();
    bool RunCurrentState();
    void SetFPS(int fps);
    void SetState(ESTATE state);
    void AddState(ESTATE stateName, CState * state);
    void CheckStates();
private:
    int m_FPS;
    ESTATE m_CurrentState;
    ESTATE m_NextState;
    std::map<ESTATE,CState *> m_States;
    CTimer m_Timer;
};