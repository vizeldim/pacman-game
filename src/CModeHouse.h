#pragma once
#include "CMode.h"

class CModeHouse : public CMode
{
public:
    void StartMode() override;
    void SwitchMode(EMODE mode) override;
    void EndMode() override;
    void Move() override;
    void CalculateTarget() override{};
    void Collide(CPlayer *) override{};
};

