Téma: Pacman
Autor: Dimitri Vizelka

## Téma z Progtestu
Implementujte klasickou hru Pacman proti počítači (můžete naimplementovat i jiné varianty).
Hra musí splňovat následující funkcionality:
Dodržování základních pravidel, sbírání bonusů, míncí (teček), třešniček, teleporty, atp.
Je implementováno několik druhů AI duchů (fungující i zároveň - např. duchové se liší barvou, znakem).
Lze načítat mapy ze souboru (bludiště + pozice bonusů a duchů).
Konfigurovatelná obtížnost hry ze souboru (rychlost duchů, délka trvání jednotlivých módů hry, pravděpodobnost přidání nového bonusu do mapy, ...)
Škálovatelná obtížnost duchů (více strategií duchů dle obtížnost, dle různého chování)

Kde lze využít polymorfismus? (doporučené)
Rozdělení hráčů: hráč, duch (různé druhy umělé inteligence)
Mód hry: klasický (duchové chodí dle svých strategií), zběsilý (duchové pronásledují hráče), nezranitelný (duchové jsou pomalejší, dají se zabít)
Herní políčko: zem, stěna, bod, bonus (změna módu), třešnička (více bodů), teleport
Uživatelské rozhraní: konzole, ncurses, SDL, OpenGL (různé varianty), …

Kde lze využít polymorfismus? (doporučené)
- generování mapy: parametry, soubor, dynamické, ...
- hráč: lokální vs síťový (implementace soutěžního módu)
- uživatelské rozhraní: konzole, barvičky, ncurses, SDL, OpenGL (různé varianty),...


## Zadání hry Pacman
Hra bude mít grafické rozhraní, proto budu využívat knihovnu SDL2.
Při spuštěni se zobrazí Menu hry.
Klávesa 'p' bude sloužit k zastaveni hry.

Načítáni mapy(pozice stěn, jídla...) bude z textového souboru.
Konkrétně pomocí znaků('#' = stěna , '.'=jidlo=mince atd).
Každý objekt hry(hráč, duch, jídlo..) bude obsažen v stejně velkém čtverci(=buňce) s pevnou velikosti.
Počet buňěk budou představovat první dvě číslice (šířka, výška) v souboru.

Dále bude existovat několik úrovní. Počet úrovní a konkrétní údaje o každém prvku hry v dané úrovni,
které se budou měnit (=> postupně ztěžovat) budu načitat ze souboru.
Konkrétně půjde o délků trvaní jednotlivých úrovni(modu).
Každá úroveň muze využívat jineho tvaru bludiště.

Pohyb pacmana i duchů je omezen stěnami. Směr pohybu pacmana budou určovat šipky na klávesnici.

Duchové jsou čtyř typů. Jsou rozlišené barvou a způsobem pohybu v jednotlivých módech hry.
Když dojde ke kolizi ducha s hráčem v normálním módu(=Chase=pronásledováni=duchové útoči na pacmana),
odečte se život pacmana v dané úrovni a vráti se na výchozi pozici. Stejně tak se duch vrátí na výchozi pozici(do domečku duchu) při střetu s pacmanem v módu 'Frightened', kdy od pacmana zpomaleně utíkají. Tento mód se zapne po snězeni jedné z 'super' minci(=větši než obyčejná mince)
Cilem pacmana je sebrat všechny mince. 


## Polymorfismus
Všechny pohybujici se objekty budou mít metodu obnoveni údajů - poloha, směr pohybu, rotace .. (Update()). Provedení dané metody se bude u jednotlivých objektů lišit.
Pohyb pacmana bude řídit hráč klávesnici. Pohyb jednotlivých duchů budou řídit AI. U jídla se bude měnit zda bylo snězeno či ne.
Pacman se navíc může otáčet a pohybujicí se objekty narozdíl od jídla mohou měnit polohu a lze je animovat metodou Animate().
Polymorfní voláni je ve třídě CObjectManager.

Dále se bude polymorfismus využívat u metody Collides. Polymorfni voláni je popsané ve třídě CMovingObject (v metodě CheckCollision). 
Metoda je přetížená  a jejím parametrem je buď CMovingObject nebo CPlayer. Chováni metody se bude lišit u CWall(nedovolí projít přes stěnu),
CFood(povolí pacmanovi sníst danou instanci jídla), CTeleport(Teleportuje instanci CMovingObject). 
