#include "GameFuncs.h"
#include "cmath"

double GameFuncs::GetDistance(const SDL_Point &first, const SDL_Point & second)
{ return sqrt(pow(second.x - first.x, 2) + pow(second.y - first.y, 2)); }

DIR GameFuncs::OpositeDir(DIR d)
{
    switch(d)
    {
        case LEFT: return RIGHT;
        case RIGHT: return LEFT;
        case UP: return DOWN;
        case DOWN: return UP;
        default: return NONE;
    }
}