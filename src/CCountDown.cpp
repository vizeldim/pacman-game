#include "CCountDown.h"
#include "CGraphicsManager.h"
#include "CInputManager.h"
#include "AppSettings.h"
CCountDown::CCountDown(unsigned int from, unsigned int interval) : m_CountFrom(from),
                                                                   m_Interval(interval)
{}

void CCountDown::Run()
{
    int cnt = (int)m_CountFrom;
    m_Timer.Start();
    while(cnt >= 0)
    {
        if(cnt == (int)m_CountFrom || m_Timer.GetTime() > m_Interval)
        {
            CGraphicsManager::GetInstance().DrawNumber(cnt,{AppSettings::WINDOW_WIDTH/2 + 25,
                                                                AppSettings::WINDOW_HEIGHT/2 - 50,
                                                                50,50});
            CGraphicsManager::GetInstance().DrawOnWindow();
            cnt--;
            m_Timer.Start();
        }
        CInputManager::GetInstance().ReadInput();
        if(CInputManager::GetInstance().m_Quit)
        { break; }
    }
}