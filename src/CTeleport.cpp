#include "CTeleport.h"
#include "CMovingObject.h"

CTeleport::CTeleport(int x, int y, int destX, int destY) : CGameObject(x, y),
                                                           m_DestX(destX),
                                                           m_DestY(destY)
{
    m_Collider.w = 2;
    m_Collider.h = 2;
    m_Collider.x += (m_DestRect.w - m_Collider.w)/2;
    m_Collider.y += (m_DestRect.w - m_Collider.w)/2;
}

void CTeleport::SetType(DIR d)
{
    if(d == LEFT)
    { m_Collider.x -= m_DestRect.w/2; m_DestX-=2;}
    else if(d == RIGHT)
    { m_Collider.x += m_DestRect.w/2; m_DestX+=2;}
    else if(d == UP)
    { m_Collider.y -= m_DestRect.w/2; m_DestY-=2;}
    else
    { m_Collider.y += m_DestRect.w/2; m_DestY+=2;}
}

void CTeleport::Collides(CMovingObject *p)
{ p -> SetPos(m_DestX,m_DestY); }