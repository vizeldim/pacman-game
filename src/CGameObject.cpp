#include "CGameObject.h"

CGameObject::CGameObject() : CObject(),
                             m_CrossWay(false),
                             m_Accessable(true)
{}

CGameObject::CGameObject(SDL_Texture *tex, int x, int y) :  CObject(tex, x, y),
                                                            m_CrossWay(false),
                                                            m_Accessable(true)
{ m_Collider = m_DestRect; }

CGameObject::CGameObject(int x, int y) : CObject(x, y),
                                         m_CrossWay(false),
                                         m_Accessable(true)
{ m_Collider = m_DestRect; }

int CGameObject::GetX()
{ return m_DestRect.x; }

int CGameObject::GetY()
{ return m_DestRect.y; }

SDL_Rect &CGameObject::GetCollider()
{ return m_Collider; }

bool CGameObject::IsAccessable() const
{ return m_Accessable; }

SDL_Point CGameObject::GetPos()
{ return {m_DestRect.x,m_DestRect.y}; }
