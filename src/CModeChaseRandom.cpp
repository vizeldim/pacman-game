#include <cmath>
#include "CModeChaseRandom.h"

void CModeChaseRandom::CalculateTarget()
{
    if(m_RndTimer.GetTime()>RANDOM_DISTANCE)
    {
        m_Target = {rand()%1000, rand()%1000};
        m_RndTimer.Start();
    }
}

void CModeChaseRandom::StartMode()
{
    CModeChase::StartMode();
    m_RndTimer.Start();
}