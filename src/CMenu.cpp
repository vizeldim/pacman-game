#include "CMenu.h"
#include "CGraphicsManager.h"
#include "CButton.h"
#include "CApplication.h"
#include "AppSettings.h"

CMenu::CMenu(const SDL_Color &bgColor, int opacity) : m_Content(nullptr),
                                                      m_GameBackground(nullptr),
                                                      m_BtnW(70),
                                                      m_BtnH(100),
                                                      m_BtnMargin(50),
                                                      m_BtnFonstSize(13),
                                                      m_BtnBgColor{255,255,255}
{
    CGraphicsManager::GetInstance().CreateDrawTexture(m_Content);
    CGraphicsManager::GetInstance().FillTexture(m_Content,bgColor,opacity);
}

CMenu::~CMenu()
{ 
	for(CButton * btn : m_Buttons)
    { delete btn; }
    
    if(m_Content!=nullptr)
    { SDL_DestroyTexture(m_Content); }
}

void CMenu::RunState()
{
    if(m_GameBackground)
    { CGraphicsManager::GetInstance().Render(m_GameBackground); }

    CGraphicsManager::GetInstance().Render(m_Content);

    for(auto btn : m_Buttons)
    { btn -> Show(); }
}

bool CMenu::ChangeState(ESTATE &state)
{
    for (auto btn : m_Buttons)
    {
        if(btn -> IsClicked())
        {
            state = btn -> GetNextState();
            return true;
        }
    }
    return false;
}

void CMenu::AddButton(const std::string& text, ESTATE nextState)
{
    SDL_Rect r;
    r.x = AppSettings::WINDOW_WIDTH/2 - m_BtnW/2;
    r.y = m_BtnMargin + (int)m_Buttons.size() * (m_BtnMargin + m_BtnH);
    r.w = m_BtnW;
    r.h = m_BtnH;

    m_Buttons.push_back(new CButton(text,r,nextState, m_BtnBgColor, m_BtnFonstSize));
}

void CMenu::SetGameBackground(SDL_Texture * tex)
{ m_GameBackground = tex; }

void CMenu::SetButtonStyle(int btnW, int btnH, int btnMargin, int fontSize, const SDL_Color & color)
{
    m_BtnW = btnW;
    m_BtnH = btnH;
    m_BtnMargin = btnMargin;
    m_BtnFonstSize = fontSize;
    m_BtnBgColor = color;
}
