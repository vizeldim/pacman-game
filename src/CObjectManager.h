#pragma once
#include <vector>
#include "CObject.h"
#include "CScore.h"
#include "EGhostType.h"
#include <fstream>
#include <map>
#include <memory>

class CPlayer;
class CScore;
class CGhost;
class CGameObject;
class CHealth;
class CMovingObject;

enum OBJECT_TYPE
{
    WALL,
    FOOD, POWERFOOD,
    PACMAN, PACMAN2, PACMAN3, PACMAN4,
    GHOST
};

class CObjectManager
{
public:
    CObjectManager();
    ~CObjectManager();

    void DrawObjects();
    void UpdateObjects();

    /*! Read Configuration (= Ghost mode times) from file */
    bool ReadConfig(const std::string &path);

    /*! Read objects from file -> allocate them -> push them into vector (m_ObjectMap) */
    bool ReadMap(const std::string& path);

    /*! Iterate through object map
     * -> Mark object as cross way if more than 2 neighbour objects are accessible
     * -> In a cross way is ghost allowed to calculate next direction based on his target
     */
    void MarkCrossWays() const;

    /*! Prepares object manager for another level
     * -> Deletes objects (except m_Player) && clears containers
     */
    void Clear();

    static std::vector<CGameObject *> m_ObjectMap;
    static int m_MapW, m_MapH;

    CPlayer * m_Player;

    SDL_Texture * GetBackground() const;
    SDL_Texture * m_Background;

    /*! Indicates if player won or loose current level */
    bool Win() const;
    bool Loose() const;

    void Restart() const;

    /*! Sets Default constant configuration => not from file */
    void SetDefaultConfig();

    /*! Pause && Unpause objects that use timers */
    void Pause();
    void UnPause();

    int GetScore() const;

private:
    /*! Creates textures for objects and stores them in container */
    void SetObjectTextures();
    std::map<OBJECT_TYPE, SDL_Texture *> m_Textures;

    /*! Adds object to object map */
    void AddObject(char o, int posX, int posY);

    /*! File stream to read objects from map txt file */
    std::ifstream m_InFile;

    /*! Stores pointers to objects */
    std::vector<CMovingObject *> m_MovingObjects;
    std::vector<CGhost *> m_Ghosts;

    /*! Total amount of food */
    int m_FoodAmount;
    SDL_Point m_GhostHouseDoor;
    std::map<GHOST_TYPE,std::vector<CGhost *>> m_Ghost;

    CGameObject * m_Blank;
};