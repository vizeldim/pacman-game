#pragma once
#include "CState.h"

class CEndGame : public CState
{
public:
    void RunState() override;
    bool ChangeState(ESTATE &state) override;
};