#pragma once
enum EGAMETITLE
{
    GAMEOVER,
    WIN,
    NEXTLEVEL,
    LAST_TITLE = NEXTLEVEL
};