#include "CState.h"
#include "CGraphicsManager.h"

void CState::PrepareState()
{ m_Timer.Start(); }

void CState::StartStateFrame()
{ CGraphicsManager::GetInstance().ClearRenderer(); }

void CState::EndStateFrame()
{ CGraphicsManager::GetInstance().DrawOnWindow(); }