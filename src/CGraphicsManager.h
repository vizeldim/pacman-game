#pragma once
#include <SDL2/SDL.h>
#include <string>
#include <vector>

class CGameObject;

class CGraphicsManager
{
public:
    static CGraphicsManager & GetInstance();
    void Delete();

    void CreateWindow();
    void SetWindowIcon(const std::string & path);
    void CreateRenderer();

    void ClearRenderer();
    void DrawOnWindow();

    void Render(SDL_Texture * tex);
    void Render(SDL_Texture * tex, const SDL_Rect &rect);
    void Render(SDL_Texture * tex, const SDL_Rect &src, const SDL_Rect &dest);
    void Render(SDL_Texture * tex, const SDL_Rect &rect, int angle);

    void CreateDrawTexture(SDL_Texture *&tex);
    void CreateDrawTexture(SDL_Texture *&tex, int width, int height);

    void FillTexture(SDL_Texture *& tex, const SDL_Color & color, int opacity);

    void DrawOnTexture(SDL_Texture *& group, const std::vector<CGameObject *>& objects);
    void HideOnTexture(SDL_Texture *& tex, const SDL_Rect &rect);

    void SetNumFont(const std::string &nums, int numSrcW, int numSrcH, const SDL_Color &color);
    void SetCharFont(const std::string &chars, int charSrcSize, int letterSpacing,const SDL_Color &color);
 
    void DrawText(SDL_Texture * tex, const std::string& text, SDL_Rect rect);
    void DrawNumber(int num, SDL_Rect rect);

    void DrawRect(const SDL_Rect & dest);
    
    void HideCell(int x, int y);

    SDL_Texture * LoadTexture(const std::string& path);

    void SetDefaultColor(const SDL_Color & color);
private:
    CGraphicsManager();
    void CheckDisplay();
    static CGraphicsManager * m_Instance;

    SDL_Window * m_Window;
    SDL_Renderer * m_Renderer;

    SDL_Texture * m_Numbers;
    SDL_Texture * m_Font;

    SDL_Rect m_FontSrcRect;
    SDL_Rect m_NumSrcRect;

    int m_LetterSpacing;
    SDL_Color m_DefaultColor;
    void UseDefaultColor();
};
