#pragma once
#include "CFood.h"

class CPowerFood : public CFood
{
public:
    CPowerFood(SDL_Texture * tex, int x, int y);

    /*! Extra -> gives player undefitable power */
    void HandleCollision(CPlayer * p) override;
};