#pragma once
#include "CModeChase.h"

class CModeChaseRandom : public CModeChase
{
public:
    void StartMode() override;
    void CalculateTarget() override;
protected:
    CTimer m_RndTimer{};
    const unsigned int RANDOM_DISTANCE = 100;
};

