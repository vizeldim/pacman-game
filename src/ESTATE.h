#pragma once

enum ESTATE
{
    MENU,
    SCORES,
    GAME,
    PAUSE,
    ENDGAME,
    GAMESTART,
    LAST = GAMESTART
};