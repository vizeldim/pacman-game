#include "CModeChaseC.h"
#include "CGhost.h"
#include "CPlayer.h"
#include "GameFuncs.h"

void CModeChaseC::CalculateTarget()
{
    if (GameFuncs::GetDistance(m_Ghost->GetPos(),m_Ghost->m_TargetPlayer->GetPos()) > (8 * CObject::GetCellSize()) )
    { CModeChase::CalculateTarget(); }
    else
    { m_Target = {0,0}; }
}