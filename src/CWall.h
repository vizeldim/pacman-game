#pragma once
#include "CGameObject.h"

class CMovingObject;
class CWall : public CGameObject
{
public:
    CWall(SDL_Texture * tex, int x, int y);
    void Collides(CMovingObject *p) override;
    void Draw() override;
    void SetColor(SDL_Color color);
private:
    SDL_Color m_Color;
};