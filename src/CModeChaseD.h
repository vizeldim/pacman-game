#pragma once
#include "CModeChaseB.h"
class CGhost;
class CModeChaseD : public CModeChaseB
{
public:
    explicit CModeChaseD(CGhost *g);
    void CalculateTarget() override;
private:
    CGhost * m_Helper;
};