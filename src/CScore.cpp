#include "CScore.h"
#include "CGraphicsManager.h"

CScore::CScore() : CObject(0, 0),
                   m_Score(0)
{}

void CScore::Draw()
{ CGraphicsManager::GetInstance().DrawNumber(m_Score,m_DestRect); }

void CScore::operator+=(int i)
{
    m_Score+=i;
    Draw();
}

void CScore::ResetScore()
{ m_Score = 0; }

int CScore::GetScore() const
{ return m_Score; }