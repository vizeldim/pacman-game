#include "CGameRestarter.h"
#include "CGame.h"

CGameRestarter::CGameRestarter(CGame *game) : m_Game(game)
{}

void CGameRestarter::RunState()
{ m_Game -> RestartGame(); }

bool CGameRestarter::ChangeState(ESTATE &state)
{
    state = MENU;
    return true;
}