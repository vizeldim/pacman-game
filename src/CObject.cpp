#include "CObject.h"
#include "CGraphicsManager.h"

int CObject::m_CellSize = 20;

CObject::CObject() : m_Texture(nullptr)
{}

CObject::CObject(int x, int y) : m_DestRect(SDL_Rect{x,y,m_CellSize,m_CellSize}),
                                 m_Texture(nullptr)
{}

CObject::CObject(SDL_Texture * tex, int x, int y) : CObject(x,y)
{ m_Texture = tex; }


void CObject::Draw()
{ CGraphicsManager::GetInstance().Render(m_Texture,m_DestRect); }

int CObject::GetCellSize()
{ return m_CellSize; }
