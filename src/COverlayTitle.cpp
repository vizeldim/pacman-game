#include "COverlayTitle.h"
#include "AppSettings.h"
#include "CGraphicsManager.h"

COverlayTitle::COverlayTitle(const SDL_Color &  bg,
                             int                opacity,
                             unsigned int       time    ) : m_Content(nullptr),
                                                            m_ShowTime(time)
{
    CGraphicsManager::GetInstance().CreateDrawTexture(m_Content);
    CGraphicsManager::GetInstance().FillTexture(m_Content,bg,opacity);
}

COverlayTitle::~COverlayTitle()
{ 
    if(m_Content!=nullptr)
    {SDL_DestroyTexture(m_Content);}
}

void COverlayTitle::Show()
{
    m_Timer.Start();
    CGraphicsManager::GetInstance().Render(m_Content);
    CGraphicsManager::GetInstance().DrawOnWindow();
}

void COverlayTitle::Wait()
{ m_Timer.Wait(m_ShowTime); }

void COverlayTitle::SetTitle(const std::string &text)
{
    m_Text = text;
    SDL_Rect rect{AppSettings::WINDOW_WIDTH/2 - (30*(int)m_Text.length())/2 ,
                  AppSettings::WINDOW_HEIGHT/2 - 30/2,
                  30,30};
    CGraphicsManager::GetInstance().DrawText(m_Content,m_Text,rect);
}
