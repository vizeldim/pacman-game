#include <stdexcept>
#include "CGraphicsManager.h"
#include "CGameObject.h"
#include "AppSettings.h"

using namespace std;

CGraphicsManager * CGraphicsManager::m_Instance = nullptr;

CGraphicsManager::CGraphicsManager() : m_Window(nullptr),
                                       m_Renderer(nullptr),
                                       m_Numbers(nullptr),
                                       m_Font(nullptr),
                                       m_FontSrcRect{0,0,0,0},
                                       m_NumSrcRect{0,0,0,0},
                                       m_LetterSpacing(0),
                                       m_DefaultColor{255,255,255}
{
    if( SDL_Init(SDL_INIT_EVERYTHING) < 0)
    { throw runtime_error("SDL could not be initialised."); }

    CreateWindow();
    CreateRenderer(); 
}

CGraphicsManager &CGraphicsManager::GetInstance()
{
    if(!m_Instance)
    { m_Instance = new CGraphicsManager(); }
    return *m_Instance;
}

void CGraphicsManager::Delete()
{
    if(m_Numbers!=nullptr){SDL_DestroyTexture(m_Numbers);}
    if(m_Font!=nullptr){SDL_DestroyTexture(m_Font);}
    if(m_Renderer!=nullptr){SDL_DestroyRenderer(m_Renderer);}
    if(m_Window!=nullptr){SDL_DestroyWindow(m_Window);}
    SDL_Quit();
    
    delete m_Instance;
}

void CGraphicsManager::CreateWindow()
{
    CheckDisplay();
    m_Window = SDL_CreateWindow( "PACMAN GAME",
                                SDL_WINDOWPOS_CENTERED,
                                SDL_WINDOWPOS_CENTERED,
                                AppSettings::WINDOW_WIDTH,
                                AppSettings::WINDOW_HEIGHT,
                                SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
    if(m_Window == nullptr)
    {throw runtime_error("SDL window could not be created.");}
    SDL_SetWindowBordered(m_Window,SDL_FALSE);
}

void CGraphicsManager::CheckDisplay()
{
    SDL_DisplayMode displayMode;
    if (SDL_GetDesktopDisplayMode(0, &displayMode) != 0)
    { throw runtime_error("Could not get display size."); }

    else if(displayMode.h < AppSettings::WINDOW_HEIGHT)
    { throw runtime_error("Your screen HEIGHT is too small for this app."); }

    else if(displayMode.w < AppSettings::WINDOW_WIDTH)
    { throw runtime_error("Your screen WIDTH is too small for this app."); }
}

void CGraphicsManager::CreateRenderer()
{
    m_Renderer = SDL_CreateRenderer(m_Window,0,SDL_RENDERER_ACCELERATED);
    if(m_Renderer == nullptr)
    {throw runtime_error("SDL RENDERER could not be created.");}

    UseDefaultColor();
}

SDL_Texture *CGraphicsManager::LoadTexture(const std::string& path)
{
    SDL_Surface * tmpObjectSurface = SDL_LoadBMP(path.c_str());
    SDL_Texture * tex = SDL_CreateTextureFromSurface(m_Renderer, tmpObjectSurface);
    SDL_FreeSurface(tmpObjectSurface);

    if(tex == nullptr)
    { {throw runtime_error("SDL Texture could not be created");} }
    return tex;
}

void CGraphicsManager::FillTexture(SDL_Texture *&tex, const SDL_Color & color, int opacity)
{
    SDL_SetRenderTarget(m_Renderer, tex);
    SDL_RenderClear(m_Renderer);
    SDL_SetTextureBlendMode(tex, SDL_BLENDMODE_BLEND);
    SDL_SetRenderDrawColor(m_Renderer,color.r,color.g,color.b,opacity);
    SDL_RenderFillRect(m_Renderer,nullptr);
    SDL_RenderPresent(m_Renderer);

    SDL_SetRenderTarget(m_Renderer, nullptr);
    UseDefaultColor();
}


void CGraphicsManager::DrawText(SDL_Texture * tex ,const string& text, SDL_Rect rect)
{
    SDL_SetRenderTarget(m_Renderer, tex);
    for(char c : text)
    {
        m_FontSrcRect.x = (c - 'A') * m_FontSrcRect.w;
        Render(m_Font,m_FontSrcRect,rect);
        //SDL_RenderCopy(m_Renderer,m_Font,&m_FontSrcRect,&rect);
        rect.x += (rect.w + m_LetterSpacing);
    }
    m_FontSrcRect.x = 0;
    SDL_RenderPresent(m_Renderer);
    SDL_SetRenderTarget(m_Renderer, nullptr);
    UseDefaultColor();
}

void CGraphicsManager::SetCharFont(const string& chars, int charSrcSize, int letterSpacing,const SDL_Color & color)
{
    m_Font = LoadTexture(chars);
    m_FontSrcRect = {0,0,charSrcSize,charSrcSize};
    SDL_SetTextureColorMod( m_Font, color.r,color.g,color.b);
    m_LetterSpacing = letterSpacing;
}

void CGraphicsManager::SetNumFont(const string& nums, int numSrcW, int numSrcH, const SDL_Color & color)
{
    m_Numbers = LoadTexture(nums);
    m_NumSrcRect = {0,0,numSrcW,numSrcH};
    SDL_SetTextureColorMod( m_Numbers, color.r,color.g,color.b);
}

void CGraphicsManager::DrawNumber(int num, SDL_Rect rect)
{
    int tmpX = rect.x - rect.w;
    int i = 0;

    while( num != 0 || i == 0 )
    {
        rect.x = tmpX;
        rect.x += i * rect.w;

        SDL_RenderFillRect(m_Renderer,&rect);

        m_NumSrcRect.x = num%10 * m_NumSrcRect.w;
        Render(m_Numbers,m_NumSrcRect,rect);
        //SDL_RenderCopy(m_Renderer,m_Numbers,&m_NumSrcRect,&rect);
        num/=10;
        i--;
    }
    m_NumSrcRect.x = 0;
}

void CGraphicsManager::Render(SDL_Texture *tex)
{
    if(tex!=nullptr)
    {SDL_RenderCopy(m_Renderer,tex,nullptr, nullptr);}
}

void CGraphicsManager::Render(SDL_Texture *tex, const SDL_Rect &rect)
{
    if(tex!=nullptr)
    {SDL_RenderCopy(m_Renderer,tex,nullptr,&rect);}
}

void CGraphicsManager::Render(SDL_Texture *tex, const SDL_Rect &rect, int angle)
{
    if(tex!=nullptr)
    { SDL_RenderCopyEx(m_Renderer, tex, nullptr, &rect, angle, nullptr, SDL_FLIP_NONE); }
}

void CGraphicsManager::Render(SDL_Texture *tex, const SDL_Rect &src, const SDL_Rect &dest)
{
    if(tex!=nullptr)
    { SDL_RenderCopy(m_Renderer,tex,&src,&dest); }
}


void CGraphicsManager::DrawOnTexture(SDL_Texture *&group, const vector<CGameObject *> &objects)
{
    if(group != nullptr)
    {
        SDL_SetRenderTarget(m_Renderer, group);
        SDL_RenderClear(m_Renderer);

        for(auto obj : objects)
        { obj->Draw(); }

        SDL_RenderPresent(m_Renderer);
        SDL_SetRenderTarget(m_Renderer, nullptr);
        UseDefaultColor();
    }
}

void CGraphicsManager::HideOnTexture(SDL_Texture *&tex, const SDL_Rect &rect)
{
    SDL_SetRenderTarget(m_Renderer, tex);
    SDL_RenderSetViewport( m_Renderer, &rect );

    SDL_RenderFillRect(m_Renderer,nullptr);

    SDL_RenderPresent(m_Renderer);
    SDL_SetRenderTarget(m_Renderer, nullptr);
}

void CGraphicsManager::CreateDrawTexture(SDL_Texture *&tex, int width, int height)
{
    tex = SDL_CreateTexture(m_Renderer,SDL_PIXELFORMAT_UNKNOWN,SDL_TEXTUREACCESS_TARGET,width, height);
    if(tex == nullptr)
    { {throw runtime_error("SDL Draw Texture could not be created");} }
}

void CGraphicsManager::CreateDrawTexture(SDL_Texture *&tex)
{ CreateDrawTexture(tex,AppSettings::WINDOW_WIDTH,AppSettings::WINDOW_HEIGHT); }

void CGraphicsManager::DrawOnWindow()
{ SDL_RenderPresent(m_Renderer); }

void CGraphicsManager::ClearRenderer()
{ SDL_RenderClear(m_Renderer); }

void CGraphicsManager::DrawRect(const SDL_Rect &dest)
{ SDL_RenderFillRect(m_Renderer,&dest); }

void CGraphicsManager::UseDefaultColor()
{
    SDL_SetRenderDrawColor( m_Renderer,
                            m_DefaultColor.r,
                            m_DefaultColor.g,
                            m_DefaultColor.b,
                            255);
}

void CGraphicsManager::HideCell(int x, int y)
{ DrawRect({x,y,CObject::GetCellSize(),CObject::GetCellSize()}); }

void CGraphicsManager::SetWindowIcon(const string & path) {
    SDL_Surface * icon = SDL_LoadBMP(path.c_str());
    if(icon != nullptr)
    { SDL_SetWindowIcon(m_Window,icon); }
}

void CGraphicsManager::SetDefaultColor(const SDL_Color &color)
{ m_DefaultColor = color; }
