#pragma once
#include <map>
#include "CObjectManager.h"
#include "CState.h"
#include "EGameTitle.h"
#include "CCountDown.h"

class COverlayTitle;
class CGame : public CState
{
public:
    CGame();
    ~CGame() override;
    
    void RunState() override;
    bool ChangeState(ESTATE &state) override;
    void PrepareState() override;
    void StartStateFrame() override;

    /*! Calls load functions of current levels map and config */
    void ConfigureState();
    void RestartGame();
    void NextLevel();

    /*! Loads file that contains path to MAP and CONFIG for each level
     *  -> file names stores in vector containers (m_LevelMaps, m_LevelConfigs)
     */
    void LoadLevelInfo(const std::string& LevelsPath);

    /*! Returns pointer Static background -> Useful for another states */
    SDL_Texture * GetBackground();

    void SetScoreFile(const std::string& path);

    void SetOverlayTitle(EGAMETITLE title, const std::string & text);
private:
    CObjectManager m_ObjectManager;

    /*! Current level of game + all level map path and configs */
    unsigned int m_CurrentLevel;
    std::vector<std::string> m_LevelMaps;
    std::vector<std::string> m_LevelConfigs;

    /*! Overlay titles -> GAMEOVER, WIN... */
    void AddGameTitle(EGAMETITLE state, COverlayTitle * title);
    std::map<EGAMETITLE,COverlayTitle *> m_Titles;

    /*! Countdown gives player chance to prepare for game
     * -> game start, after pause
     */
    CCountDown m_CountDown;

    /*! Saves score into file
     * -> Holds only 3 values
     * @param scoreValue = value to store
     */
    void SaveScore(int scoreValue);
    std::string m_ScoreFilePath;
};
