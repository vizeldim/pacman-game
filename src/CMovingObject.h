#pragma once
#include <SDL2/SDL.h>
#include "EDir.h"
#include "CGameObject.h"

class CMovingObject : public CGameObject
{
public:
    CMovingObject(SDL_Texture * tex, int x, int y);
    virtual void Update() = 0;

    /*! Move back once */
    void Back();

    void SetSpeed(double m_Speed);

    /*! Change current position */
    void SetPos(double x, double y);
    virtual void DefaultPos();

    /*! Current Direction */
    DIR m_Direction;

    void SetChangableBg(SDL_Texture * tex);
protected:
    virtual void CheckCollisions();
    virtual void HandleCollision(CGameObject * object);
    void Colliding(CGameObject * obj);

    /*! Function moves object by speed in current direction */
    void Move();

    /*! Temporary direction before change */
    DIR m_OldDir;

    /*! Current Speed */
    double m_Speed;

    /*! Detects if moving object is stuck == TRUE */
    bool m_Stuck;

    /*! Default positions */
    int m_X, m_Y;

    /*! Current positions */
    double m_PosX,m_PosY;

    /*!
     * Temporary real last position
     * -> Last position of m_Dest.x and m_Dest.y before change
     */
    int TEMPX,TEMPY;

    SDL_Texture * m_ChangableBg;
};
