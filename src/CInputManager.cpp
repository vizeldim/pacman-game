#include "CInputManager.h"

CInputManager * CInputManager::m_Instance = nullptr;

CInputManager::CInputManager() : m_Pause(false),
                                 m_Quit(false),
                                 m_Direction(NONE)
{}

CInputManager &CInputManager::GetInstance()
{
    if(!m_Instance)
    { m_Instance = new CInputManager(); }
    return *m_Instance;
}

void CInputManager::Delete()
{ delete m_Instance; }

void CInputManager::Reset()
{
    m_Pause = false;
    m_Direction = NONE;
}


void CInputManager::ReadInput()
{
    while(SDL_PollEvent(&e))
    {
        switch (e.type) {
            case SDL_QUIT:
                m_Quit = true;
                break;
            case SDL_KEYDOWN:
                ReadKey();
                break;
            default:
                break;
        }
    }
}

void CInputManager::ReadKey()
{
    switch( e.key.keysym.sym )
    {
        case SDLK_UP:
            m_Direction = UP;
            break;
        case SDLK_DOWN:
            m_Direction = DOWN;
            break;
        case SDLK_LEFT:
            m_Direction = LEFT;
            break;
        case SDLK_RIGHT:
            m_Direction = RIGHT;
            break;
        case SDLK_ESCAPE:
            m_Quit = true;
            break;
        case SDLK_p:
            m_Pause = !m_Pause;
        default:
            break;
    }
}

DIR CInputManager::GetDir()
{ return m_Direction; }

bool CInputManager::IsMouseInArea(const SDL_Rect &rect)
{
    int mX, mY;
    SDL_GetMouseState(&mX, &mY);
    return  mX <= rect.x+rect.w &&
            mX >= rect.x &&
            mY <= rect.y+rect.h &&
            mY >= rect.y;
}
