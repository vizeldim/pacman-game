# PACMAN GAME

## Ported on web as webassembly

Published on gitalb pages: [click here to play](https://vizeldim.gitlab.io/pacman-game/out/)

## Introduction
This game is a Pacman clone. It was created as a Semestral project at Czech technical university. Application is written in c++ and uses SDL2 library.

## How to play
- The player - pacman is controlled by keybord arrows. -> Up, Down, Left, Right.
- To win current level player has to eat all the food in map and the purpose is to achieve the best score.
- Player = pacman becomes indefeatable after eating powerfood -> Ghosts change their mode to frightened -> Player recieves power to eat ghost and gets 100 points.

## User Interface
- For pause -> Click 'p' key. 
- Menu buttons are clickable by mouse.
- In scores menu are shown three best results of the whole time.

## Assignment (in Czech)
+ Implementujte klasickou hru Pacman proti počítači (můžete naimplementovat i jiné varianty).

### Hra musí splňovat následující funkcionality:
+ Dodržování základních pravidel, sbírání bonusů, míncí (teček), třešniček, teleporty, atp.
+ Je implementováno několik druhů AI duchů (fungující i zároveň - např. duchové se liší barvou, znakem).
+ Lze načítat mapy ze souboru (bludiště + pozice bonusů a duchů).
+ Konfigurovatelná obtížnost hry ze souboru (rychlost duchů, délka trvání jednotlivých módů hry, pravděpodobnost přidání nového bonusu do mapy, ...)
+ Škálovatelná obtížnost duchů (více strategií duchů dle obtížnost, dle různého chování)

### Kde lze využít polymorfismus? (doporučené)
+ Rozdělení hráčů: hráč, duch (různé druhy umělé inteligence)
+ Mód hry: klasický (duchové chodí dle svých strategií), zběsilý (duchové pronásledují hráče), nezranitelný (duchové jsou pomalejší, dají se zabít)
+ Herní políčko: zem, stěna, bod, bonus (změna módu), třešnička (více bodů), teleport
+ Uživatelské rozhraní: konzole, ncurses, SDL, OpenGL (různé varianty), ...


## External files
 All external files are stored in examples folder
 
#### /examples/maps:
+ '#' = wall
+ '.' = food
+ 'o' = powerfood
+ 'U' = ghost house door, default(0,0)
+ 'P' = player == PACMAN position, default(0,0)
+ 'B' = Ghost Blinky -> His target is player
+ 'R' = Ghost Pinky -> His target are two cells near player based on players direction
+ 'I' = Ghost Inky -> His target is based on blinkys and players direction
+ 'C' = Ghost Clyde -> His target is player, but when he is too close to him, changes target to (0,0)

#### /examples/config:
- 4 Values for each ghost type
1) Amount of eaten food by player until is ghost allowed to live ghost house
2) Time in miliseconds which ghost spends in ghost house after being eaten
3) Time in miliseconds represents duration of ghost frighten mode => player is allowed to eat ghost 
4) Time in miliseconds represents duration of ghost scatter mode => ghost stops following player and his target is set to one of four corners of game area

++ Fifth line contains time in miliseconds. After spending this time in chase mode, each ghost changes his mode to scatter.
 (min 0 value, max 10 values), only positive values are allowed. 

## Commands
+ make compile    -> Compile the whole project, create executable file
+ make run		  -> Run executable
+ make doc 	   	  -> Generate doxygen documentation into /doc folder
+ make / make all -> Generate documentation and compile the project
+ make clean	  -> Remove all generated files

## Used sources
- https://cs.wikipedia.org/wiki/Pac-Man 
- http://programujte.com/clanek/2011010500-chovani-duchu-ve-hre-pac-man-cast-1/ 
- http://programujte.com/clanek/2011010500-chovani-duchu-ve-hre-pac-man-cast-2/

