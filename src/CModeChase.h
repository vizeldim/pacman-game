#pragma once
#include "CMode.h"

class CModeChase : public CMode
{
public:
    CModeChase();
    void StartMode() override;
    void EndMode() override;
    void CheckSwitch() override;
    void CalculateTarget() override;
    void Collide(CPlayer *) override;
private:
    bool m_CanScatter;
};
