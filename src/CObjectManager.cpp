#include <fstream>
#include <sstream>
#include <iostream>
#include "CObjectManager.h"
#include "CPlayer.h"
#include "CWall.h"
#include "CTeleport.h"
#include "CPowerFood.h"
#include "CGraphicsManager.h"
#include "CModeChase.h"
#include "CModeChaseB.h"
#include "CModeChaseC.h"
#include "CModeChaseD.h"
#include "CGhost.h"
#include "ObjectSettings.h"
#include "AppSettings.h"

using namespace std;

vector<CGameObject *> CObjectManager::m_ObjectMap;
int CObjectManager::m_MapW = 0;
int CObjectManager::m_MapH = 0;

CObjectManager::CObjectManager() : m_Player(nullptr),
                                   m_Background(nullptr),
                                   m_FoodAmount(0),
                                   m_GhostHouseDoor{0,0},
                                   m_Blank(new CGameObject(0,0))
{
    SetObjectTextures();
    m_Player = new CPlayer(m_Textures[PACMAN], 0, 0, AppSettings::PACMAN_HEALTH);
    m_Player -> SetChangableBg(m_Background);
    m_Player -> AddAnimation(m_Textures[PACMAN2]);
    m_Player -> AddAnimation(m_Textures[PACMAN3]);
    m_Player -> AddAnimation(m_Textures[PACMAN4]);
    m_MovingObjects.push_back(m_Player);
    CGraphicsManager::GetInstance().SetWindowIcon(AppSettings::PACMAN_IMG);
}

CObjectManager::~CObjectManager()
{
    Clear();
    delete m_Blank;
    delete m_Player;
    SDL_DestroyTexture(m_Background);

    for(auto tex : m_Textures)
    { 
        if(tex.second!=nullptr)
        {SDL_DestroyTexture(tex.second);}
    }
}

void CObjectManager::AddObject(char o, int posX, int posY)
{
    CWall * w;
    CFood * f;
    CTeleport * t;
    CGhost * g;
    CPowerFood * sf;
    CGameObject * door;
    int dest;
    switch(o)
    {
        case '#':
            w = new CWall(m_Textures[WALL], posX, posY);
            m_ObjectMap.push_back(w);
            break;
        case '$':
            w = new CWall(m_Textures[WALL], posX, posY);
            w -> SetColor({0 ,255,0});
            m_ObjectMap.push_back(w);
            break;
        case '.':
            f = new CFood(m_Textures[FOOD], posX, posY);
            m_ObjectMap.push_back(f);
            ++m_FoodAmount;
            break;
        case 'o':
            sf = new CPowerFood(m_Textures[POWERFOOD], posX, posY);
            m_ObjectMap.push_back(sf);
            ++m_FoodAmount;
            break;
        case 'P':
            m_Player -> SetPlayer(m_Textures[PACMAN],posX,posY);
            m_ObjectMap.push_back(m_Blank);
            break;
        case 'B':
            g = new CGhost(m_Textures[GHOST], posX, posY, new CModeChase());
            m_MovingObjects.push_back(g);
            m_ObjectMap.push_back(m_Blank);
            m_Ghosts.push_back(g);
            m_Ghost[BLINKY].push_back(g);

            g -> m_Corner = {0,0};
            g -> SetColor(AppSettings::BLINKY_COLOR, AppSettings::FRIGHTENED_COLOR);
            break;
        case 'R':
            g = new CGhost(m_Textures[GHOST], posX, posY, new CModeChaseB());
            m_MovingObjects.push_back(g);
            m_ObjectMap.push_back(m_Blank);
            m_Ghosts.push_back(g);
            m_Ghost[PINKY].push_back(g);

            g -> m_Corner = {AppSettings::WINDOW_WIDTH,0};
            g -> SetColor(AppSettings::PINKY_COLOR, AppSettings::FRIGHTENED_COLOR);

            break;
        case 'C':
            g = new CGhost(m_Textures[GHOST], posX, posY, new CModeChaseC());
            m_MovingObjects.push_back(g);
            m_ObjectMap.push_back(m_Blank);
            m_Ghosts.push_back(g);
            m_Ghost[CLYDE].push_back(g);

            g -> m_Corner = {AppSettings::WINDOW_WIDTH,AppSettings::WINDOW_HEIGHT};
            g -> SetColor(AppSettings::CLYDE_COLOR, AppSettings::FRIGHTENED_COLOR);

            break;
        case 'I':
            g = new CGhost(m_Textures[GHOST], posX, posY, new CModeChaseD(m_Ghosts[0]));
            m_MovingObjects.push_back(g);
            m_ObjectMap.push_back(m_Blank);
            m_Ghosts.push_back(g);
            m_Ghost[INKY].push_back(g);

            g -> m_Corner = {0,AppSettings::WINDOW_HEIGHT};
            g -> SetColor(AppSettings::INKY_COLOR, AppSettings::FRIGHTENED_COLOR);
            break;
        case 'T':
            char c;
            if((m_InFile >> dest) && (m_InFile >> c) && dest > 0 && dest < m_MapW * m_MapH)
            {
                t = new CTeleport(posX,posY,(dest%m_MapW) * CObject::GetCellSize(), ((dest/m_MapW)+2) *  CObject::GetCellSize());
                if(c=='L')
                {t -> SetType(LEFT);}
                else if(c=='R')
                {t -> SetType(RIGHT);}
                else if(c=='U')
                {t -> SetType(UP);}
                else if(c=='D')
                {t -> SetType(DOWN);}
                m_ObjectMap.push_back(t);
            }
            else
            { m_ObjectMap.push_back(m_Blank); }
            break;
        case 'U':
            door = new CGameObject(0,0);
            door->m_CrossWay = true;
            m_GhostHouseDoor = {posX,posY};
            m_ObjectMap.push_back(door);
            break;
        default:
            m_ObjectMap.push_back(m_Blank);
            break;
    }
}

void CObjectManager::DrawObjects()
{
    for(CObject * obj : m_MovingObjects)
    { obj -> Draw(); }
}

void CObjectManager::UpdateObjects()
{
    for(CMovingObject * obj : m_MovingObjects)
    { obj -> Update(); }
}

void CObjectManager::MarkCrossWays() const
{
    for(int i = 0; i < (int)m_ObjectMap.size(); i++)
    {
        if (m_ObjectMap[i]!=nullptr && m_ObjectMap[i]->IsAccessable())
        {
            bool left = false, right = false, up = false, down = false;
            if (((i - m_MapW) >= 0) && (m_ObjectMap[i-m_MapW]!=nullptr) && (m_ObjectMap[i-m_MapW]->IsAccessable()))
            { up = true; }

            if (((i + m_MapW) < (int)m_ObjectMap.size()) && (m_ObjectMap[i+m_MapW]!=nullptr) && (m_ObjectMap[i+m_MapW]->IsAccessable()))
            { down = true; }

            if (((i - 1) >= 0) && (m_ObjectMap[i - 1]!=nullptr) && (m_ObjectMap[i - 1]->IsAccessable()))
            { left = true; }

            if (((i + 1) < (int)m_ObjectMap.size()) && (m_ObjectMap[i + 1]!=nullptr) && (m_ObjectMap[i + 1]->IsAccessable()))
            { right = true; }
            if ((left || right) && (up || down)) { m_ObjectMap[i]->m_CrossWay = true; }
        }
    }
}

bool CObjectManager::ReadMap(const string& path)
{
    m_InFile.open(path, std::ifstream::in);
    cout << "READ MAP " << endl;
    if(!m_InFile.is_open())
    {
        cout << "Cant Open map file." << path << endl;
        m_InFile.close();
        return false;
    }

    char fragment;
    int r, c;
    if( !(m_InFile >> r >> c) || (r <=0) || (c <=0) ||
        (r * CObject::GetCellSize() > AppSettings::WINDOW_WIDTH) ||
       ((c + 1) * CObject::GetCellSize()) > AppSettings::WINDOW_HEIGHT)
    {
        cout << "Map does not fit in window -> "<< path << endl;
        m_InFile.close();
        return false;
    }

    m_MapW = r;
    m_MapH = c;
    m_Player -> SetSHposX(r * CObject::GetCellSize(), 0);


    for(int j = 0; j < r*2; j++)
    { m_ObjectMap.push_back(new CGameObject()); }

    for(int i = 2; i <= c+1; i++)
    {
        for(int j = 0; j < r; j++)
        {
            if (m_InFile.eof())
            { cout << "Invalid Map." << endl; }
            m_InFile >> fragment;
            AddObject(fragment,j * CObject::GetCellSize(),i * CObject::GetCellSize());
        }
    }
    char end;
    m_InFile >> end;
    if (!m_InFile.eof())
    { cout << "Some More characters or lines in map file." << endl; }
    m_InFile.close();
    MarkCrossWays();

    for(int j = 0; j <= r; j++)
    { m_ObjectMap.push_back(new CGameObject()); }

    for(CGhost * g : m_Ghosts)
    {
        g -> m_TargetPlayer = m_Player;
        g -> m_HouseDoor = m_GhostHouseDoor;
        g -> SetChangableBg(m_Background);
        m_Player -> m_Enemies.push_back(g);
        g -> SetSpeed(AppSettings::MOVING_OBJECT_SPEED);
    }
    m_Player -> SetSpeed(AppSettings::MOVING_OBJECT_SPEED);
    cout << "Draw Map to texture" << endl;
    CGraphicsManager::GetInstance().DrawOnTexture(m_Background,m_ObjectMap);

    return true;
}

bool CObjectManager::ReadConfig(const string &path)
{
    ifstream infile(path);
    if(!infile.is_open())
    {
        cout << "Cant Open config file. " << path << " Default config was set." << endl;
        infile.close();
        return false;
    }

    string line;
    for(int i = 0; i < 4; ++i)
    {
        int mc[4];
        if(!getline(infile, line))
        {
            cout << "Not enough values in config file "<< path <<  " Default config was set." << endl;
            infile.close();
            return false;
        }

        istringstream iss(line);

        for (int & j : mc)
        {
            if(!(iss >> j) || j < 0 )
            {
                cout << "Not enough values in config file or negative value"<< path << "on line --> " << i <<  ". Default config was set." << endl;
                infile.close();
                return false;
            }
        }

        for(CGhost * g : m_Ghost[(GHOST_TYPE)i])
        { g -> Configure(mc); }
    }

    int scatterTime[10];
    if(!infile.eof() && getline(infile, line) && !infile.fail() )
    {
        istringstream iss(line);
        int i = 0;
        int time = 0;
        while(iss>>time && time >= 0 && i < 10)
        { scatterTime[i++] = time; //cout << time << endl;
        }
        scatterTime[i] = -1;
    }
    else
    {scatterTime[0] = -1;}

    for(const auto& ghosts : m_Ghost)
    {
        for(CGhost * g : ghosts.second)
        { g -> SetScatter(scatterTime); }
    }

    infile.close();
    return true;
}

void CObjectManager::SetDefaultConfig()
{
    int i = 0;
    for(const auto& ghosts : m_Ghost)
    {
        for(CGhost * g : ghosts.second)
        {
            g -> SetScatter(AppSettings::DEFAULT_SCATTER_TIME);
            g -> Configure(AppSettings::DEFAULT_GHOST_CONFIG[i]);
        }
        ++i;
    }
}

void CObjectManager::SetObjectTextures()
{
    CGraphicsManager::GetInstance().CreateDrawTexture(m_Background);
    m_Textures.insert(make_pair(WALL,CGraphicsManager::GetInstance().LoadTexture(AppSettings::WALL_IMG)));
    m_Textures.insert(make_pair(FOOD,CGraphicsManager::GetInstance().LoadTexture(AppSettings::FOOD_IMG)));
    m_Textures.insert(make_pair(GHOST,CGraphicsManager::GetInstance().LoadTexture(AppSettings::GHOST_IMG)));
    m_Textures.insert(make_pair(POWERFOOD,CGraphicsManager::GetInstance().LoadTexture(AppSettings::POWER_FOOD_IMG)));
    m_Textures.insert(make_pair(PACMAN,CGraphicsManager::GetInstance().LoadTexture(AppSettings::PACMAN_IMG)));
    m_Textures.insert(make_pair(PACMAN2,CGraphicsManager::GetInstance().LoadTexture(AppSettings::PACMAN2_IMG)));
    m_Textures.insert(make_pair(PACMAN3,CGraphicsManager::GetInstance().LoadTexture(AppSettings::PACMAN_IMG)));
    m_Textures.insert(make_pair(PACMAN4,CGraphicsManager::GetInstance().LoadTexture(AppSettings::PACMAN_IMG)));
}

void CObjectManager::Clear()
{
    cout << "CLEARED " << endl;
    for(CGameObject * obj : m_ObjectMap)
    { if(obj != m_Blank){delete obj;} }
    m_ObjectMap.clear();

   for(CGhost * g : m_Ghosts)
   { delete g; }

    m_Ghosts.clear();
    m_Ghost.clear();
    m_MovingObjects.clear();

    m_Player -> ResetInfo();
    m_FoodAmount = 0;
    m_MovingObjects.push_back(m_Player);
}

SDL_Texture *CObjectManager::GetBackground() const
{ return m_Background; }

bool CObjectManager::Win() const
{ return m_Player->EatenAmount() == m_FoodAmount;}

bool CObjectManager::Loose() const
{ return m_Player->GetHealth() <= 0; }

void CObjectManager::Restart() const
{ m_Player -> ResetScore(); }

void CObjectManager::Pause()
{
    for(CGhost * g : m_Ghosts)
    { g -> Pause(); }
}

void CObjectManager::UnPause()
{
    for(CGhost * g : m_Ghosts)
    { g -> UnPause(); }
}

int CObjectManager::GetScore() const
{ return m_Player -> GetScore(); }
