#include "CHealth.h"
#include "CGraphicsManager.h"

CHealth::CHealth() : CObject(0,0),
                     m_CurrentLifes(0),
                     m_DefaultLifes(0)
{}

void CHealth::Draw()
{
    int tmpX = m_DestRect.x;
    for(int i = 0; i < m_CurrentLifes; ++i)
    {
        CObject::Draw();
        m_DestRect.x += m_DestRect.w;
    }
    m_DestRect.x = tmpX;
}

void CHealth::operator--()
{
    --m_CurrentLifes;
    CGraphicsManager::GetInstance().HideCell(m_DestRect.x + m_CurrentLifes * CObject::GetCellSize(), m_DestRect.y);
}

int CHealth::GetLifes() const
{ return m_CurrentLifes; }

void CHealth::SetTex(SDL_Texture *tex)
{ m_Texture = tex; }

void CHealth::SetDefault()
{ m_CurrentLifes = m_DefaultLifes; }

void CHealth::SetLifes(int lifes)
{
    m_CurrentLifes = lifes;
    m_DefaultLifes = lifes;
}