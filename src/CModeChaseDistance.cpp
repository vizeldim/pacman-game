#include "CModeChaseDistance.h"
#include "CGhost.h"
#include "CPlayer.h"
#include "GameFuncs.h"

void CModeChaseDistance::CheckSwitch()
{
    CModeChase::CheckSwitch();
    if( (GameFuncs::GetDistance(m_Ghost->GetPos(),m_Ghost->m_TargetPlayer->GetPos())) < (5 * CObject::GetCellSize()) )
    { SwitchMode(SCATTER); }
}