#include "CWall.h"
#include "CPlayer.h"

CWall::CWall(SDL_Texture * tex, int x, int y) : CGameObject(tex, x, y),
                                                m_Color{255,255,255}
{ m_Accessable = false; }

void CWall::Collides(CMovingObject *p)
{ p->Back(); }

void CWall::SetColor(SDL_Color color)
{ m_Color = color; }

void CWall::Draw()
{
    SDL_SetTextureColorMod(m_Texture,m_Color.r, m_Color.g, m_Color.b);
    CObject::Draw();
}