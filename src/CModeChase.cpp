#include <iostream>
#include "CModeChase.h"
#include "CObjectManager.h"
#include "CGhost.h"
#include "CPlayer.h"

using namespace std;

CModeChase::CModeChase() : m_CanScatter(true)
{}

void CModeChase::Collide(CPlayer *p)
{
    p -> DecHealth();
    p -> DefaultPos();

    for(auto gh : p->m_Enemies)
    { gh->ChangeMode(HOME); }
}

void CModeChase::StartMode()
{
    m_Ghost -> ModeChanged();
    m_Timer.Continue();
}

void CModeChase::EndMode()
{ m_Timer.Pause(); }

void CModeChase::CheckSwitch()
{
    if(m_CanScatter && m_Timer.GetTime() > m_Duration)
    {
        cout << "SCATTER TIME :)" << m_Duration << endl;
        SwitchMode(SCATTER);
        m_CanScatter = m_Ghost -> ChangeDuration(m_Duration);
    }
}

void CModeChase::CalculateTarget()
{ m_Target = m_Ghost->m_TargetPlayer->GetPos(); }