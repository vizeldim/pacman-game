#include <iostream>
#include "CGhost.h"
#include "CObjectManager.h"
#include "CPlayer.h"
#include "CMode.h"
#include "CModeChase.h"
#include "CModeFrightened.h"
#include "CModeHouse.h"
#include "CModeSleep.h"
#include "CModeScatter.h"
#include "CGraphicsManager.h"
#include "CModeChaseDistance.h"

using namespace std;

CGhost::CGhost(SDL_Texture *tex, int x, int y, CModeChase *mode) : CMovingObject(tex, x, y),
                                                                   m_CurrentMode(SLEEP),
                                                                   m_TargetPlayer(nullptr),
                                                                   m_Corner{0,0},
                                                                   m_HouseDoor{0,0},
                                                                   m_CurrentDuration(0)
{
    if(mode == nullptr)
    { m_Mode.insert(make_pair(CHASE,new CModeChaseDistance())); }
    else
    { m_Mode.insert(make_pair(CHASE,mode)); }

    m_Mode.insert(make_pair(FRIGHTENED,new CModeFrightened()));
    m_Mode.insert(make_pair(SCATTER,new CModeScatter()));
    m_Mode.insert(make_pair(HOME,new CModeHouse()));
    m_Mode.insert(make_pair(SLEEP,new CModeSleep()));

    for(auto m : m_Mode)
    { m.second->SetGhost(this); }

    m_Collider.x += m_Collider.w/3;
    m_Collider.y += m_Collider.h/3;
    m_Collider.w /= 3;
    m_Collider.h /= 3;
}

CGhost::~CGhost()
{
    for(auto m : m_Mode)
    { delete m.second; }
}

void CGhost::Update()
{
    DrawBgPart();
    CheckChangeMode();

    if (((TEMPX != m_DestRect.x || TEMPY!= m_DestRect.y)||
        (m_CurrentMode==HOME || m_CurrentMode==SLEEP)) &&
        (m_DestRect.x % m_DestRect.w == 0 &&
         m_DestRect.y % m_DestRect.w == 0))
    {
        ChangeDir();
        TEMPX = m_DestRect.x;
        TEMPY = m_DestRect.y;
    }
    Move();
    CheckCollisions();
}

void CGhost::ChangeDir()
{ m_Mode[m_CurrentMode]->Move(); }

void CGhost::Collides(CPlayer *p)
{ m_Mode[m_CurrentMode]->Collide(p); }

void CGhost::RunAway()
{
    switch(m_Direction)
    {
        case LEFT:  m_Direction = RIGHT; break;
        case RIGHT: m_Direction = LEFT;  break;
        case UP:    m_Direction = DOWN;  break;
        case DOWN:  m_Direction = UP;    break;
        default: break;
    }
}

void CGhost::Draw()
{
    m_Mode[m_CurrentMode] -> UseColor(m_Texture);
    CObject::Draw();
}

void CGhost::SetColor(const SDL_Color & mainColor, const SDL_Color & frightenColor)
{
    m_Mode[CHASE]->SetColor(mainColor);
    m_Mode[HOME] -> SetColor(mainColor);
    m_Mode[SLEEP] -> SetColor(mainColor);
    m_Mode[FRIGHTENED] -> SetColor(frightenColor);
    m_Mode[SCATTER] -> SetColor({255,255,255});
}

void CGhost::GetOut()
{
    DrawBgPart();
    SetPos(m_HouseDoor.x, m_HouseDoor.y);
}

void CGhost::Slow()
{ m_Speed /=2 ; }

void CGhost::CheckChangeMode()
{ m_Mode[m_CurrentMode] -> CheckSwitch(); }

void CGhost::SetMode(const EMODE& mode)
{
    m_CurrentMode = mode;
    m_Mode[m_CurrentMode] -> StartMode();
}

void CGhost::ChangeMode(EMODE m)
{ m_Mode[m_CurrentMode] -> SwitchMode(m); }

void CGhost::Fast()
{
    m_Speed *= 2;
    double x = m_DestRect.x - m_DestRect.x%m_DestRect.w;
    double y = m_DestRect.y - m_DestRect.y%m_DestRect.w;

    while( x < m_DestRect.x)
    { x += m_Speed; }

    while( y < m_DestRect.y)
    { y += m_Speed; }

    SetPos(x,y);
}

void CGhost::Configure(const int modeChangers[4])
{
    for(int i = 0; i < 4; ++i)
    { m_Mode[(EMODE)i] -> SetDuration(modeChangers[i]); }
}

void CGhost::SetScatter(const int scatterTimes[10])
{
    int i = 0;
    while(scatterTimes[i]!=-1 && i < 10)
    {
        //cout << scatterTimes[i] << endl;
        m_ScatterTimes.push_back(scatterTimes[i]);
        ++i;
    }
    if(i!=0)
    { m_Mode[CHASE]->m_Duration = m_ScatterTimes[m_CurrentDuration++];}
}

bool CGhost::ChangeDuration(unsigned int &duration)
{
    if(m_CurrentDuration < m_ScatterTimes.size())
    {
        duration = m_ScatterTimes[m_CurrentDuration++];
        return true;
    }
    return false;
}

void CGhost::ModeChanged()
{ TEMPX = -1; }

void CGhost::Pause()
{ m_Mode[m_CurrentMode]->Pause(); }

void CGhost::UnPause()
{ m_Mode[m_CurrentMode]->UnPause(); }

void CGhost::DrawBgPart()
{
    if(m_ChangableBg!=nullptr)
    { CGraphicsManager::GetInstance().Render(m_ChangableBg,m_DestRect,m_DestRect); }
}
